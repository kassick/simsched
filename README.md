# Descrição

Este código é um esqueleto para a execução de códigos de escalonamento de processos a partir de traços. O código é baseado em simulação de eventos -- i.e. criação de processos, ticks de escalonador, etc. são eventos que são executados num momento pré-determinado.

É possível implementar vários códigos de escalonamento independentes e adicioná-los ao simulador sem que um interfira com o outro. Desta forma, é possível testar diferenças entre dois algoritmos de escalonamento usando o mesmo executável e arquivos de traço.

# Tipos Básicos

- **struct process** -- Definido em sim_process.h : Estrutura que contém informações sobre um processo. Esta estrutura contém um vetor com a duração de cada CPU burst do processo, bem como um vetor da duração de cada I/O burst (cpu_bursts, io_bursts), além de um contador indicando qual o *burst* **atual** sendo simulado.
- **struct processor** -- Definido em sim_processor.h : Estrutura que representa cada processador do simulador. Ela armazena informações a respeito do processador simulado (e.g. *affinity*) e as variáveis do OS que possuem uma instância por processador (e.g. *current*).
- **struct computer** -- Definido em sim_computer.h : Estrutura com informações a respeito do *computador simulado*. Nela são armazenados um vetor de processadores (*processors* , com *n_processors* elementos) e um vetor de processos (*processes*, com *n_processes* elementos).
- **struct scheduler** -- Definido em sim_scheduler.h : Estrutura com informações sobre o escalonador. Ela possui ponteiros para as funções do escalonador (*enqueue*, *dequeue*, *tick*, *sched*, *dispatch*, ...), além de um ponteiro chamado *priv*, que pode apontar para qualquer coisa que o escalonador necessite internamente.

# Arquitetura

O simulador inicia sua execução lento um arquivo de traços de processos e um de afinidade de processadores. A seguir ele inicializa o escalonador a ser utilizado e agenda o *fork* do processo **init** (PID 0). A partir daí, ele passa a executar cada evento agendado até que não hajam mais eventos. Ao fim da execução, ele apresenta algumas informações sobre estatísticas de execução.

O simulador possui 4 componentes principais:

- Simulador de Eventos
- Leitor de Traços
- Simulação do Sistema Operacional
- Escalonador

## Simulador de Eventos

O simulador de eventos está implementado no arquivo *sim_cpu_engine.c*.

Um evento é uma estrutura com os seguintes campos:

- *ts*: *timestamp*, A *hora* em que o evento deve ser executado
- *handle*: Uma função que deve ser chamada para tratar o evento
- *priv*: Um ponteiro para algo que a função pode usar (e.g. estrutura com parâmetros, um ponteiro para o *processo* referente ao evento, etc.)
- *destroy*: Função a ser chamada antes da estrutura de eventos ser liberada. Ela recebe como parãmetro o valor de *priv* e tem a tarefa de liberar a memória apontada por *priv* quando necessário.

Cada processador possui uma lista de eventos (tipo *event_t*, funções de lista em list.h). Para criar um novo evento, deve-se usar a função *sim_create_event*, que irá retornar um novo *event_t*. O código deve atribuir os campos *ts*, *handle*, *priv* e *destroy* corretamente e depois invorcar *sim_add_event*. Esta função adiciona o novo evento em uma lista garantindo o ordenamento por *ts*, ou seja, ao adicionar o evento

    (5, fifo_tick, NULL, NULL)

à lista

    {   (0, evt_create_process, 0xdeadbeef, NULL), 
        (6, evt_fork_process, 0xfeebdaed, NULL) 
    }
a lista de eventos resultante será

    {
        (0, evt_create_process, 0xdeadbeef, NULL),
        (5, fifo_tick, NULL, NULL),
        (6, evt_fork_process, 0xfeebdaed, NULL)
    }

Se dois eventos em uma lista possuem o mesmo *ts*, a ordem na lista é igual à ordem de inserção. Isto é, ao inserir o evento ``(5, evt_fork_process, 0xf00baa0, NULL)s`` à lista

    {
        (0, evt_create_process, 0xdeadbeef, NULL),
        (5, evt_fork_process, 0xfeebdaed, NULL)
    }
    
a lista resultante será

    {
        (0, evt_create_process, 0xdeadbeef, NULL),
        (5, evt_fork_process, 0xfeebdaed, NULL),
        (5, evt_fork_process, 0xf00baa0, NULL)
    }

Cada evento *ocorre* em uma CPU. Isto é, cada CPU possui uma lista de eventos e, no momento que um evento ocorre, é *sabido* qual CPU *gerou* o evento. Cada evento recebe como parâmetro a CPU atual. Adicionalmente, a *struct computer* da simulação possui um campo *cur_cpu* que é atualizado com o *cpu_id* da CPU atual.

A simulação dos eventos ocorre na função *sim_main_loop*. A simulação começa no tempo **0**. O *main_loop* executa os seguintes passos:

- Localiza o evento com menor timestamp entre todas as CPUs
- Remove este evento da lista
- Ajusta o *current_time* para o *ts* do evento atual
- Invoca a função *handle* do evento, passando como parâmetro a *cpu* do evento e *priv*
- Se o evento possui uma função *destroy*, chama ela com *priv*
- Localiza o próximo evento, repete

Por simplicidade, cada evento executa em 0 unidades de tempo -- i.e. a execução de um evento não modifica *current_time*. Qualquer coisa que tenha a noção de início e fim deve ser quebrada em dois eventos -- um evento que marca o início e outro que marca o fim. Um exemplo disso é a execução de um processo em uma CPU por um determinado tempo: Algum evento marca o início da execução do processo e cria um novo evento, com *ts* igual a *current_time* + *timeslice*, que irá simular o fim da execução do processo -- por exemplo, o processo é *preemptado* ou o processo *bloqueia*.

É importante lembrar que um evento pode criar novos eventos e adicioná-los às listas de eventos de qualquer CPU. É permitido adicionar eventos com *ts* igual a *current_time* -- isto é, um evento que deve acontecer *agora* (após processar todos eventos adicionados anteriormente com o mesmo timestamp). Isto é quase equivalente a invocar a função *handle* diretamente. Não é permitido criar eventos com tempo *anterior* a *current_time* -- isto irá gerar um erro e parar a execução.

É também permitido inserir eventos diretamente no início da lista para garantir que o próximo evento executado na CPU será o evento adicionado, e.g.
    
    event_t *wakeup_evt = sim_new_event();

    wakeup_evt->ts = sim_current_time();
    wakeup_evt->handle = evt_reschedule;
    wakeup_evt->priv = NULL;
    wakeup_evt->destroy = NULL;

    list_add(&wakeup_evt->to_list, &cpu->event_list);

Funções importantes implementadas no simulador de eventos são

- *sim_init* -- Inicializa a simulação
- *sim_main_loop* -- Simula os eventos existentes nas listas. Termina quando não há mais eventos ou quando algum evento chamou sim_stop()
- *sim_create_event* -- Cria um novo evento.
- *sim_add_event* -- Adiciona um novo evento à lista de eventos de uma CPU
- *sim_add_event_no_dup* -- Adiciona um novo evento à lista de eventos de uma CPU, *desde que não seja um evento duplicado*. Um evento é considerado duplicado na lista quando existe outro evento com mesmo *ts*, *handle* e *priv* (a mesma função, invocada no mesmo tempo, com o mesmo parâmetro). Quando o evento é duplicado, ele é destruído. 
- *sim_cancel_event* -- Cancela a execução de um determinado evento -- isto é, remove da lista e destrói a memória do evento
- *sim_cancel_event_no_free* -- Cancela o evento, mas não libera a memória associada
- *find_pending_event* -- Localiza um evento. Os campos utilizados para localizar um evento são definidor no parâmetro *flags*. A função permite localizar eventos por *timestamp*, *handle* e *priv*, bem como qualquer combinação desses.
- *find_pending_event_in_cpu* -- Idêntica a *find_pending_event*, mas localiza por CPU.

**IMPORTANTE:** Após chamar *sim_add_event_no_dup*, deve-se testar se o retorno da função é igual a 0. Se for igual a 0, o evento  não era duplicado. Caso contrário, o evento era duplicado e foi destruído. Neste caso, o ponteiro para a estrutura de evento não é mais válido e sua utilização (mesmo em printf) pode gerar problemas de memória.

## Leitor de Traços

O leitor de traços está implementado em *sim_trace.c*. Possui duas funções importantes:

- *parse_trace* -- Lê o traço de processos de um arquivo e devolve o vetor de processos e o número de elementos no vetor
- *parse_affinity* -- Lê um arquivo que descreve o *affinity* entre todos os processadores

O traço de processos tem o seguinte formato:
    
    <nprocs>
    <pid0> <parent_pid0> <parent_burst0> <after0> <prio0> <n_cpu_bursts0> <n_io_bursts0>
    <cpu_burst_0_len> <io_burst_0_len>
    <cpu_burst_1_len> <io_burst_1_len>
    ...
    <cpu_burst<n_cpu_bursts0 - 1>_len> -1
    <pid1> <parent_pid1> <parent_burst1> <after1> <prio1> <n_cpu_bursts1> <n_io_bursts1>
    <cpu_burst_0_len> <io_burst_0_len>
    <cpu_burst_1_len> <io_burst_1_len>
    ...
    <cpu_burst<n_cpu_bursts1 - 1>_len> -1
    ...
    <pid<nprocs - 1>> ...

O traço a seguir descreve 2 processos, cada um com 2 CPU bursts. O processo 0 cria o processo 1 em seu segundo CPU burst depois de 5 unidades de tempo

    2
    0 0 0 0 100 2 1
    100 1000
    100 -1
    1 0 1 5 100 2 1
    50 500
    100 -1

É importante lembrar que o simulador não se importa com a unidade de tempo utilizada, nem nos traços tampouco nos parâmetros do escalonador. É responsabilidade do aluno garantir que as unidades estejam concisas.

Um arquivo de *affinity* descreve o nível de afinidade entre cada par de CPUs. A implementação de exemplo usa *inteiros* para descrever a afinidade. Se for necessário descrever afinidade com algum outro tipo -- e.g. float -- modifique o vetor *affinity* em *process* e a função *parse_affinity*.

O formato de um arquivo de affinity é o seguinte:

    <n_cpus_file>
    <affinity_cpu_0_to_0>
    <affinity_cpu_0_to_1>
    ...
    <affinity_cpu_0_to_<n_cpus_file-1>>
    <affinity_cpu_1_to_0>
    <affinity_cpu_1_to_1>
    ...
    <affinity_cpu_1_to_<n_cpus_file-1>>
    ...
    <affinity_cpu_<n_cpus_file-1>_to_<n_cpus_file-1>>

Um arquivo de affinity para 2 processadores em que a afinidade entre processadores diferentes é 1/10 da afinidade entre o mesmo processador teria o seguinte conteúdo:

    2
    10
    1
    1
    10

isto é, as afinidades entre as CPUs 0 e 0 e entre 1 e 1 são 10 e, entre CPUs diferentes, 1.

A afinidade entre processadores é armazenada no campo *affinity* de cada CPU. Por exemplo, se é necessário saber a afinidade entre a CPU atual de um evento e a CPU 6, o código pode acessar o valor

    int affinity = c->processes[c->cur_cpu].affinity[6]

## Simulação do Sistema Operacional

O simulador possui funções que representam *syscalls* e *interrupts* que seriam executados durante a execução de um conjunto de processos. Estas função são implementadas como *eventos* e se encontram em *sim_common_events.c*.

- *evt_fork_process*: Invocada quando um novo processo é criado. Equivalente à parte da *syscall* **clone** que informa ao escalonador que uma nova estrutura descrevendo um processo está disponível para executar.
- *evt_block_process*: Invocada quando um processo deve *bloquear*. Equivalente a qualquer parte do sistema operacional que force o processo a bloquear (e.g. I/O, bloquear por mutex/semáforo, esperar por rede, etc.).
- *evt_exit_process*: Ocorre quando um processo *termina*. Equivalente à *sycall* **exit**, que informa o escalonador que um determinado processo não existe mais e não deve ser escalonado.
- *evt_wakeup_process*: Ocorre quando um processo deixou de estar bloqueado.
- *evt_reschedule*: Evento que indica que o escalonador deve ser chamado.

Estas funções devem lidar com a natureza simulada do sistema. Por exemplo, um processo, quando ele ganha o processador, ele pode executar por um *timeslice*, ajustado pelo escalonador. A função *evt_reschedule* deve então agendar um evento *perder a CPU* ao fim desse *timeslice*. Porém, um evento *fork* ou *wakeup* pode acontecer antes de o processo perder a CPU, e se o escalonador for preemptivo, ele pode decidir executar o novo processo ao invés do processo que estava na CPU. Neste caso, o evento de *perder a CPU* do processo que estava na CPU deve ser **cancelado** para garantir o funcionamento correto.

Note que isto não tem relação com o código de *escalonamento*, é apenas uma característica da maneira como o simulador opera.

Note também que, apesar de a função *evt_reschedule* chamar o *tick* do escalonador, não é necessário que o escalonador selecione um processo diferente do atual. Um escalonador FIFO não preemptivo, por exemplo, apenas irá selecionar um processo diferente quando o processo na CPU bloquear. Neste caso, quando um novo processo é criado, o escalonador é chamado, porém a decisão de escalonamento segue a mesma: manter o processo atual.

## Escalonadores

Um escalonador deve implementar as seguintes funções:

- *enqueue* : Uma função que adiciona um processo READY recém criado à lista de escalonamento.
- *dequeue*: Uma função que remove um processo FINISHED da lista de escalonamento.
- *block*: Uma função que informa ao escalonador que um processo que estava READY, agora está BLOCKED e não pode mais ser escalonado.
- *wake_up*: Uma função que informa ao escalonador que um processo que estava BLOCKED, agora está READY e pode voltar a ser escalonado.
- *sched*: Uma função que decide qual processo deve ganhar a CPU
- *dispatch*: Uma função que põe um processo a executar em uma CPU
- *tick*: Uma função que ocorre quando o sistema operacional precisa de uma decisão de escalonamento.


A função *tick* deve atualizar o *timeslice* do processo atual e invocar o escalonador. Em seguida, ela deve invocar *dispatch* com este processo selecionado, indicando que o processo ganhou a CPU.

Deve-se lembrar que uma possível decisão de escalonamento é *nenhum processo* -- e.g. não há processos READY na fila. Neste caso, a função *dispatch* informa que o processador se torna *idle*.


### Criando um novo escalonador

Para criar um novo escalonador, deve-se criar dois novos arquivo (e.g. sched_dummy.c e sched_dummy.h). No arquivo .c, deve-se adicionar as funções listadas acima, além de uma função *dummy_create_scheduler*.

A função *dummy_create_scheduller* deve ter a assinatura

```c
struct scheduler *
    dummy_create_scheduler( struct computer * c, 
                            int argc,
                            char**argv)
```

Ela é responsável por criar uma nova *struct scheduler*, definir os campos da struct com as funções do escalonador, além de criar os dados privados ao escalonador (apontados em *priv*). Ela também deverá configurar um scheduler para cada CPU -- por exemplo, a própria instância do scheduler -- e definir o campo *sched_priv* de cada CPU, quando necessário.

A função *create_dummy* retorna uma *struct scheduler* que é atribuída ao campo *scheduler* em *struct computer*. Se você atribuir instâncias diferentes de escalonadores para cada CPU, lembre-se que o *computador* também terá um escalonador, ainda que este não seja usado.

O arquivo *sched_dummy.h* deverá, obrigatoriamente, possuir a definição da função *create_dummy*. As definições das outras funções não são necessárias neste arquivo.

Para que o simulador enxergue o novo escalonador, é necessário adicioná-lo à lista de escalonadores. Inclua o arquivo *sched_dummy.h* com ``#include`` no arquivo *sim_main.c* e adicione uma entrada ao vetor ``available_schedulers``:

```c
const struct sched_opt_entry available_schedulers[] = {
    {"dummy", "DUMMY does nothing", dummy_create_scheduler, NULL},
    {"fifo", "First-in First-out scheduller", fifo_create_scheduler, NULL},
    {"rr", "Round-Robin scheduller", rr_create_scheduler, rr_print_help},
    {NULL, NULL, NULL},
};
```

Adicione o arquivo *sched_dummy.c* às dependências de *simsched* (dentro de *src/CMakeLists.txt*) e compile.

Ao executar ``./simsched --list-sched``, ele deverá informar que *dummy* é um escalonador suportado.

    Available schedullers:
    dummy : DUMMY does nothing
    fifo  : First-in First-out scheduller
    rr    : Round-Robin scheduller

### Passando parâmetros para um escalonador

Alguns escalonadores podem precisar de parâmetros externos para executar -- e.g. Round-robin possui um *quanta*. A função de criação do escalonador recebe parâmetros *argc* e *argv* que podem ser utilizados durante a criação do escalonador para definir estes parâmetros.

O código principal do escalonador irá eliminar quaisquer opções utilizadas pelo simulador em si. Por exemplo, ao executar

```
./simsched -a affinity.txt -t tracefile.txt -s rr --quanta 100
```

no momento de criação do escalonador Round-robin, *argv* possuirá apenas os elementos

```c
{ "./simsched", "--quanta", "100" }
```

Os parâmetros podem ser interpretados usando funções para *parsing* de parâmetros (*getopt* ou outra) ou podem ser lidos diretamente, e.g.

```c
    if (argc > 1) {
        quanta = atol(argv[1]);
    }
```

Para fornecer uma mensagem de *help* (``./simsched --help``), deve-se definir uma função de help que será referenciada no vetor ``available_schedulers``:

```c
void dummy_print_help() {
    printf("    --ignore-me <value> :  "
           "Ignored parameter as DUMMY DOES NOTHING\n");
}
```

A função ``dummy_print_help()`` deve ter sua definição em *sched_dummy* para que *sim_main.c* possa enxergar a função. Para que a função *help* do simulador chame a função de help do escalonador *dummy*, referencia ela no campo *help* da entrada em *available_schedullers*?

```c
const struct sched_opt_entry available_schedulers[] = {
    {"dummy", "DUMMY does nothing", dummy_create_scheduler, dummy_print_help},
    {"fifo", "First-in First-out scheduller", fifo_create_scheduler, NULL},
    {"rr", "Round-Robin scheduller", rr_create_scheduler, rr_print_help},
    {NULL, NULL, NULL, NULL},
};
```

# Usando o Simulador

## Geração de Traços

Os traços são gerados pelo programa gen_trace.py. Para gerar traços, edite o arquivo e modifique o vetor *tests* para incluir a geração de algum teste específico. Cada entrada deste vetor possui 7 campos:

1. Número de Processos a serem gerados
2. Nome do arquivo de trace a ser gerado
3. Distribuição para randomizar *priodidade*
3. Distribuição para randomizar *fanout*
4. Distribuição para randomizar *interfork interval*
5. Distribuição para randomizar *quantidade de CPU bursts*
6. Distribuição para randomizar *duração de CPU bursts*
7. Distribuição para randomizar *duração de I/O burst*

Cada *distribuição* é uma tupla com um **nome** e uma tupla de **parâmetros**. O nome deve referenciar uma função **meta_** que irá criar uma função que devolve números randômicos de acordo com a distribuição específica (ver ``DISTS_DICT``).  Por exemplo, a tupla ("normal", (10, 5)) irá chamar a função ``meta_normal`` com os parâmetros 10 e 5 -- média e desvio padrão, respectivamente. Esta função retorna uma função que, ao ser chamada, devolve números aleatórios de acordo com a curva normal descrita. Consultar Wolfram Alpha para informações sobre outras curvas e parâmetros.

![Curva Normal $\mu=10$, $\sigma=5$, <http://www.wolframalpha.com/input/?i=plot+normal+distribution+sigma%3D5+mu%3D10>](normal_10_5.png)

Os parêmetros *randomizados* são os seguintes:

- **prioridade**: Prioridade para cada processo 
- **fan-out**: Quantidade de processos que cada processo cria
- **inter-fork interval**: Quantidade de *bursts* entre dois forks em um processo
- **quantidade de CPU bursts**: Quantidade total de CPU bursts que um processo terá
* **duração de CPU bursts**: Quanto tempo de duração para cada CPU burst
* **duração de I/O bursts**: Quanto tempo de duração para cada I/O burst

Cada vez que o gerador de traços "criar" um processo, ele irá produzir novos valores randômicos de acordo com as funções definidas em *tests*. Isto é, a geração de traços não irá gerar traços idênticos a cada vez que é chamada.

## Simulação

Para executar o simulador com um arquivo de traços *trace_0.txt*, 1 CPU e o escalonador Round-Robin, execute
    ./simsched -t trace_0.txt -n 1 -s rr

O simulador irá produzir a saída

```
Running with 1 processors, affinity is:
0-0 = 1
Created Round Robin scheduler with quanta 10
[CPU  0 FORK at          0] 0 -> 0
        [RR ENQUEUE] at 0 pid 0
        [RR Wake Up Processor 0]
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                            Simulation starts NOW
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
[CPU  0 RESCHEDULE at          0] Current -1 with (non up to date) timeslice -1
        [RR TICK] at 0
        [RR sched] at 0 for cpu 0
        [RR dispatch] at 0 to process 0
        Reschedule -1 -> 0, timeslice: 10 sigma_timeslice: 0
                Last ready: 0
                sigma ready: 0
        Adding new events for process 0
        Process 0 will RESCHEDULE at 10
[CPU  0 RESCHEDULE at         10] Current 0 with (non up to date) timeslice 10
        [RR TICK] at 10
        [RR sched] at 10 for cpu 0
        [RR dispatch] at 10 to process 0
        Reschedule 0 -> 0, timeslice: 10 sigma_timeslice: 10
                Last ready: 10
                sigma ready: 0
        Adding new events for process 0
        Process 0 will RESCHEDULE at 20
...
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Simulation Finished
Current time: 876611
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Process 0: 0 migrations	4565 waiting
Process 1: 0 migrations	821 waiting
Process 2: 0 migrations	7928 waiting
Process 3: 0 migrations	11475 waiting
Process 4: 0 migrations	11527 waiting
Process 5: 0 migrations	7930 waiting
Process 6: 0 migrations	5113 waiting
Process 7: 0 migrations	6789 waiting
Process 8: 0 migrations	3489 waiting
Process 9: 0 migrations	5440 waiting
```
