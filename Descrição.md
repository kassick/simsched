---
header-includes:
    - \usepackage{ifxetex}
    - \ifxetex
    -       \usepackage{tgtermes}
    - \else
    -       \usepackage{times}
    - \fi
title: Sistemas Operacionais II -- 2º Trabalho Prático 
author: Prof. Rodrigo Virote **Kassick**
date: 18/05/2015
---


# Descrição

Implementar um algoritmo de escalonamento utilizado em algum sistema operacional moderno, considerando ambientes multiprocessados. Exemplos são o CFS (Linux) e ULE (FreeBSD), assim como os escalonadores encontrados no Solaris, Windows (Família NT, Win-XP em diante), Darwin, etc.


# 1ª Etapa

A primeira etapa do trabalho consiste em escolher um algoritmo de escalonamento  e entregar um relatório (1 página, formato PDF) descrevendo suas características gerais -- objetivo do algoritmo, o tipo de *workload* que ele objetiva otimizar, os cenários de uso, etc. -- bem como uma descrição geral do seu funcionamento interno -- estruturas de dados usadas, categorias de processos consideradas, como lida com prioridades, etc.

# 2ª Etapa

A segunda etapa consiste na implementação do algoritmo escolhido na primeira etapa. O algoritmo será avaliado com simulação através de arquivos com traços sintéticos simulando diversos *workloads*. O objetivo é avaliar como o algoritmo distribui o tempo de CPU entre os processos.

Uma implementação de referência em linguagem C será fornecida quando da entrega da primeira etapa do trabalho, exemplificando a implementação de um algoritmo de escalonamento clássico. Fica a critério do aluno utilizar a implementação de referência como base ou criar uma nova implementação em  C ou outra linguagem (e.g. Java, Javascript, Python, Haskell, C++, C#, F#, ...).

Os traços irão conter informações sobre os processos (prioridades, tipo de trabalho, momento de criação, etc.), além de todos os *bursts* de CPU e de I/O. O algoritmo de escalonamento deve considerar a ocorrência de cada um destes bursts a partir da criação do processo. O algoritmo não pode, no entanto, utilizar informações sobre os *bursts* futuros para tomar decisões -- se houver algum mecanismo de *previsão* de tempo do próximo burst de CPU, esse mecanismo deve ser implementado juntamente com o algoritmo de escalonamento.

Se o algoritmo escolhido exigir informações que não estão presentes nos traços conforme definidos na implementação de referência, o aluno deve contatar o professor para definir se o formato de traço será estendido ou se os casos específicos que necessitam da informação extra devem ser tratados de outra forma.

# Avaliação do Algoritmo

O código entregue deverá apresentar dados sobre o escalonamento dos processos, tal como *waiting time* mínimo, máximo e médio para cada classe de processos e diferentes prioridades.

# Avaliação do Trabalho

A nota do aluno no trabalho irá consistir de:
1. Compreensão do funcionamento geral do algoritmo
1. Implementação correta
1. Compreensão do comportamento do algoritmo

# Datas Importantes

1. Entrega da 1ª etapa: **25/05/2015**
1. Aulas de Acompanhamento: **08/06/2015** e **15/06/2015**
1. Entrega e Apresentação: **29/06/2015**
