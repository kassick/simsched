/*
 * ===========================================================================
 *
 *       Filename:  sim_cpu_engine.h
 *
 *    Description:  Discrete event simulation for a simulated multi-cpu
 *    computer
 *
 *        Version:  1.0
 *        Created:  27-05-2015 09:19:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIM_CPU_ENGINE_H__
#define __SIM_CPU_ENGINE_H__ 1
#include "list.h"

typedef long long int sim_time_t;
struct computer;
struct processor;

// FUnção chamada pelo engine -- retorna o valor que o relógio virtual deve
// avançar
typedef void (*handle_event_t)(struct processor *, void *);

// Função que destroy os dados privados do evento
typedef void (*destroy_event_t)(void *);

// Estrutura de um evento
struct event {
    struct list_head to_list;
    sim_time_t ts;           // QUando este evento ocorre
    void * priv;                // Dados privados para handle
    handle_event_t handle;      // FUnção que trata o evento
    destroy_event_t destroy;    // Função que destroy o evento
};

typedef struct event event_t;

void sim_init(struct computer * c);
sim_time_t sim_current_time();
struct processor * sim_cpu();
struct computer * sim_computer();

void sim_stop();

struct event * sim_new_event();

void sim_cancel_event(event_t *evt);
void sim_cancel_event_no_free(event_t *evt);

#define FIND_PENDING_BY_TS (1<<1)
#define FIND_PENDING_BY_HANDLE (1<<2)
#define FIND_PENDING_BY_PRIV (1<<3)
event_t * find_pending_event(struct computer * c, sim_time_t ts, handle_event_t handle_fun, void * priv, int flags);

event_t * find_pending_event_in_cpu(struct computer * c, int cpu_id, sim_time_t ts, handle_event_t handle_fun, void * priv, int flags);


void sim_add_event(struct list_head *list, struct event * new);
int sim_add_event_no_dup(struct list_head *__list, struct event * new);


typedef const char * const (*handle_fun_to_str)(void*);
void sim_dump_events(struct computer * c, handle_fun_to_str fun_name);

void sim_dump_events_cpu(struct processor * cpu, handle_fun_to_str fun_name);

void sim_main_loop(struct computer * c, handle_fun_to_str name_fun);
#endif
