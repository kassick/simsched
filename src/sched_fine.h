/*
 * ===========================================================================
 *
 *       Filename:  sched_fifo.h
 *
 *    Description:  FIFO scheduler
 *
 *        Version:  1.0
 *        Created:  16-05-2015 17:42:35
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#ifndef __SCHED_FINE_H__
#define __SCHED_FINE_H__ value

#define FINE_DEFAULT_QUANTA (10)
struct computer;
struct scheduler * fine_create_scheduler(struct computer * c, int argc, char**argv);
void fine_print_help();


#endif
