/*
 * ===========================================================================
 *
 *       Filename:  sim_cpu_engine.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  16-05-2015 14:52:11
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "sim_cpu_engine.h"
#include "sim_computer.h"
#include "sim_processor.h"


/*---------------------------------------------------------------------------
 *  Private variables to the simulation engine
 *--------------------------------------------------------------------------*/

// Simulation Current Time
sim_time_t current_time;

// Current event being handled
event_t *cur_event;

// Flag that forces the simulation to stop
int __sim_stop;

// Simulation computer
struct computer * __sim_computer = NULL;


/*---------------------------------------------------------------------------
 *  Function: sim_current_time
 *  Description: Returns the current time of the simulation
 *  Return: The current time
 *--------------------------------------------------------------------------*/
sim_time_t sim_current_time() {
    return current_time;
}


/*---------------------------------------------------------------------------
 *  Function: sim_computer
 *  Description: Returns the computer used for the simulation
 *--------------------------------------------------------------------------*/
struct computer * sim_computer() {
    return __sim_computer;
}


/*---------------------------------------------------------------------------
 *  Function: sim_cpu
 *  Description: Returns the CPU that generated the current event
 *--------------------------------------------------------------------------*/
struct processor * sim_cpu() {
    if (sim_computer()->cur_cpu < 0)
        return NULL;

    return &sim_computer()->processors[sim_computer()->cur_cpu];
}



/*---------------------------------------------------------------------------
 *  Function: sim_stop
 *  Description: Forces the simulation to stop, no more events will be
 *  processed
 *--------------------------------------------------------------------------*/
void sim_stop() {
    __sim_stop = 1;
}


/*---------------------------------------------------------------------------
 *  Function: sim_new_event
 *  Description: Allocates memory for a new event. User must set ts, handle,
 *  destroy and priv before adding to the lists
 *  Return: Newsly allocated event_t
 *--------------------------------------------------------------------------*/
struct event * sim_new_event() {
    struct event* tmp = malloc(sizeof(struct event));

    if (tmp) {
        tmp->handle = NULL;
        tmp->destroy = NULL;
        tmp->priv = NULL;
        tmp->ts = 0;
        tmp->to_list.next = tmp->to_list.prev = &(tmp->to_list); // empty list
    } else {
        fprintf(stderr, "ERROR: Not enough memory\n");
    }

    return tmp;
}


/*---------------------------------------------------------------------------
 *  Function: sim_add_event
 *  Description: Add an event to the event list pointed by __list. The list is
 *  ordered by timestamp. A new event with the same timestamp will be added
 *  after all events with the same timestamp, i.e.
 *     sim_add_event( { ts: e1, ts: e2, (ts+2): e3}, ( (ts), e_new) ) 
 *       --->    {ts: e1, ts: e2, ts: e_new, (ts+2): e3}
 *  Param __list: An event list (i.e. cpu->event_list)
 *  Param new: New event to add
 *--------------------------------------------------------------------------*/
void sim_add_event(struct list_head *__list, struct event * new) {
    struct list_head * e, 
                     * next = __list;
    
    // list empty: a == list at the end of the loop
    // list !empty: a == element which should follow new
    list_for_each(e, __list) {
        struct event *evt = list_entry(e, event_t, to_list);
        if (evt->ts > new->ts) {
            next = e;
            break;
        }
    }
    
    // Add new before a
    list_add_tail(&new->to_list, next);
}


/*---------------------------------------------------------------------------
 *  Function: sim_add_event_no_dup
 *  Description: Adds and event to the list IFF there is not another event
 *  with the same function and priv at the same time. A return of 1 means that
 *  the parameter is no longer valid!
 *  
 *  Param __list: event list to add the event
 *  Param new: new event
 *  Return: 1 if event was duplicated, 0 otherwise
 *--------------------------------------------------------------------------*/
int sim_add_event_no_dup(struct list_head *__list, struct event * new) {
    struct list_head * e, 
                     * next = __list;
    
    // list empty: a == list at the end of the loop
    // list !empty: a == element which should follow new
    list_for_each(e, __list) {
        struct event *evt = list_entry(e, event_t, to_list);

        // Check if this is a duplicate
        if (evt->ts     == new->ts      &&
            evt->handle == new->handle  &&
            evt->priv   == new->priv)
        {
            // Drop duplicated event
            if (new->destroy)
                new->destroy(new->priv);
            free(new);
            return 1;
        }

        if (evt->ts > new->ts) {
            next = e;
            break;
        }
    }
    
    // Add new before a
    list_add_tail(&new->to_list, next);

    return 0;
}


/*---------------------------------------------------------------------------
 *  Function: sim_cancel_event_no_free
 *  Description: Cancels an event, but does not destroy the priv or free the
 *  event struct
 *--------------------------------------------------------------------------*/
void sim_cancel_event_no_free(event_t *evt) {
    if (evt == cur_event) {
        fprintf(stderr, "ERROR: Removing current event from queue!\n");
        exit(1);
    }
    list_del(&evt->to_list);
}


/*---------------------------------------------------------------------------
 *  Function: sim_cancel_event
 *  Description: Cancels an event. Calls destroy(priv) and free(evt)
 *  Param evt: The event to cancel
 *--------------------------------------------------------------------------*/
void sim_cancel_event(event_t *evt) {
    if (evt == cur_event) {
        fprintf(stderr, "ERROR: Removing current event from queue!\n");
        exit(1);
    }
    sim_cancel_event_no_free(evt);
    if (evt->destroy)
        evt->destroy(evt->priv);
    //else if (next_event->priv)
    //    fprintf(stderr, "WARNING: Event had priv but did not have destroy\n");

    free(evt);
}

/*---------------------------------------------------------------------------
 *  Function: find_pending_event_in_cpu
 *  Description: Finds any event pending that matches the parameters in a
 *  specific CPU event list
 *  Param c: The simulated computer
 *  Param cpu_id: The number of the CPU to look for events
 *  Param ts: Timestamp of the event
 *  Param handle_fun: Handle function for the event
 *  Param priv: priv field of the event
 *  Param flags: Informs which fields to filter for. Mask of
 *  FIND_PENDING_BY_TS, FIND_PENDING_BY_HANDLE, FIND_PENDING_BY_PRIV
 *
 * See find_pending_event for how to use this function
 *--------------------------------------------------------------------------*/
event_t * find_pending_event_in_cpu(struct computer * c, 
                                    int cpu_id,
                                    sim_time_t ts,
                                    handle_event_t handle_fun,
                                    void * priv,
                                    int flags) {
    struct list_head * cur, *tmp;
    if (!( (cpu_id >= 0) && (cpu_id < c->n_processors))) {
        fprintf(stderr, "Looking for something in inexistent CPU %d\n", cpu_id);
        exit(1);
    }

    list_for_each_safe(cur, tmp, &(c->processors[cpu_id].event_list)) {
        struct event * e = list_entry(cur, event_t, to_list);
        if (    (!(flags&FIND_PENDING_BY_TS)      || (e->ts == ts)) &&
                (!(flags&FIND_PENDING_BY_HANDLE)  || (e->handle == handle_fun)) &&
                (!(flags&FIND_PENDING_BY_PRIV)    || (e->priv == priv)) )
            {
                return e;
            }
        }
    return NULL;
}


/*---------------------------------------------------------------------------
 *  Function: find_pending_event
 *  Description: Finds any event pending that matches the parameters
 *  Param c: The simulated computer
 *  Param ts: Timestamp of the event
 *  Param handle_fun: Handle function for the event
 *  Param priv: priv field of the event
 *  Param flags: Informs which fields to filter for. Mask of
 *  FIND_PENDING_BY_TS, FIND_PENDING_BY_HANDLE, FIND_PENDING_BY_PRIV
 *
 *  To look for an event only by it's timescamp, use
 *      find_pending_event(c, a_ts, NULL, NULL, FIND_PENDING_BY_TS);
 *
 *  To look for an event by handle and priv, use
 *      find_pending_event(c, -1, a_handle_fun, a_priv, FIND_PENDING_BY_PRIV|FIND_PENDING_BY_HANDLE);
 *--------------------------------------------------------------------------*/
event_t * find_pending_event(   struct computer * c, 
                                sim_time_t ts,
                                handle_event_t handle_fun,
                                void * priv,
                                int flags) {
    int i;
    for (i = 0; i < c->n_processors; i++) {
        event_t * tmp = find_pending_event_in_cpu(c, i, ts, handle_fun, priv, flags);
        if (tmp)
            return tmp;
    }

    return NULL;
}


/*---------------------------------------------------------------------------
 *  Function: sim_dump_events_cpu
 *  Description: List the pending events for a given CPU
 *  Param cpu: The CPU
 *  Param fun_name: Function that returns the name of the handle function --
 *  can be NULL
 *--------------------------------------------------------------------------*/
void sim_dump_events_cpu(struct processor * cpu, handle_fun_to_str fun_name)
{
    struct list_head * e;
    list_for_each(e, &cpu->event_list) {
        const char  *name_handle = NULL,
                    *name_destroy = NULL;
        char ptr_handle[32],
             ptr_destroy[32];
        event_t * evt = list_entry(e, event_t, to_list);

        if (fun_name) {
            name_handle = fun_name(evt->handle);
            name_destroy = fun_name(evt->destroy);
        }

        if (!name_handle) {
            sprintf(ptr_handle, "%p", evt->handle);
            name_handle = ptr_handle;
        }

        if (!name_destroy) {
            sprintf(ptr_destroy, "%p", evt->destroy);
            name_destroy = ptr_destroy;
        }

        fprintf(stderr, "[CPU %4d %10lld] %s (priv=%p, destroy=%s)\n",
                cpu->cpu_id,
                evt->ts, name_handle, evt->priv, name_destroy);
    }
}


/*---------------------------------------------------------------------------
 *  Function: sim_dump_events
 *  Description: List all pending events in a simulation
 *  Param c: The simulated computer
 *  Param fun_name: Function that returns the name of the callback
 *--------------------------------------------------------------------------*/
void sim_dump_events(struct computer * c, handle_fun_to_str fun_name) {
    int i;

    fprintf(stderr, 
            "-------------------------------------------------------------------------------\n"
            "                              Dumping Events\n");

    for (i = 0; i < c->n_processors; i++) {
        sim_dump_events_cpu(&c->processors[i], fun_name);
    }
    fprintf(stderr, 
            "-------------------------------------------------------------------------------\n");
}

/*---------------------------------------------------------------------------
 *  Function: find_next_event
 *  Description: Finds the next event in the queue that must be processed in
 *  main_loop
 *  Param ncpus: Number of CPUS
 *  Param cpus: Array of CPUs
 *  Param out_cpu: (out) the CPU with the earliest event
 *  Return: The earliest event in all the lists or NULL if all lists empty
 *--------------------------------------------------------------------------*/
struct event * find_next_event(int ncpus, struct processor * cpus, struct processor** out_cpu) {
    int i;
    int cpu_i = -1;
    int first_cpu;
    struct event * next_event = NULL;
    sim_time_t next_sim_time = LLONG_MAX;

    // If not first time, start from last CPU
    if (*out_cpu)
        first_cpu = (*out_cpu)->cpu_id;
    else
        first_cpu = 0;

    for (i = first_cpu; i < first_cpu + ncpus; i++) {
        int cpu_id = i % ncpus;
        if (!list_empty(&cpus[cpu_id].event_list)) {
            struct event * e = list_first_entry(&cpus[cpu_id].event_list,
                                                struct event,
                                                to_list);
            if (e->ts < next_sim_time) {
                next_event = e;
                next_sim_time = e->ts;
                cpu_i = cpu_id;
            }
        }
    }

    if (next_event)
        *out_cpu = &(cpus[cpu_i]);

    return next_event;
}

/*---------------------------------------------------------------------------
 *  Function: sim_main_loop
 *  Description: Dispatch pending events
 *--------------------------------------------------------------------------*/
void sim_main_loop(struct computer * c, handle_fun_to_str name_fun) {
    int i;
    struct event * next_event;
    struct processor *cpu = NULL;
    __sim_stop = 0;
    __sim_computer = c;
    c->cur_cpu = -1;

    next_event = find_next_event(c->n_processors, c->processors, &cpu);

    while (next_event && !__sim_stop) {

#ifdef SIMSCHED_DEBUG
#warning COMPILING WITH DEBUB
        sim_dump_events(c, name_fun);
#endif


        // Just make sure we don't ever go back in time
        if (next_event->ts < current_time) {
            fprintf(stderr, "NO TIME TRAVELERS ALLOWED\n");
            exit(1);
        }

        cur_event = next_event;
        c->cur_cpu = cpu->cpu_id;
        current_time = next_event->ts;

        // Removes the event from the list before handling
        // Avoids issues with find_pending
        list_del(&next_event->to_list);

        // "Simula" o evento
        if (next_event->handle)
            next_event->handle(cpu, next_event->priv);
        else
            fprintf(stderr, "ERROR: Event did not have a handle function\n");
   
        // Evento "desaparece"
        if (next_event->destroy)
            next_event->destroy(next_event->priv);
        //else if (next_event->priv)
        //    fprintf(stderr, "WARNING: Event had priv but did not have destroy\n");

        free(next_event);
        next_event = find_next_event(c->n_processors, c->processors, &cpu);
    }
}



/*---------------------------------------------------------------------------
 *  Function: sim_init
 *  Description: Initialize everything needed for the simulation
 *  @param c: The simulated computer
 *--------------------------------------------------------------------------*/
void sim_init(struct computer * c) {
    int i;

    __sim_computer = c;
    c->cur_cpu = 0;

    for (i = 0; i < c->n_processors; i++) {
        // make list empty
        c->processors[i].event_list.next = 
                    c->processors[i].event_list.prev = 
                                &c->processors[i].event_list;
    }

    current_time = 0;
}
