/*
 * ===========================================================================
 *
 *       Filename:  sim.h
 *
 *    Description:  Header para as estruturas do simulador
 *
 *        Version:  1.0
 *        Created:  16-05-2015 13:49:12
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIMSCHED_H__
#define __SIMSCHED_H__ value

#include "list.h"
#include "sim_scheduler.h"
#include "sim_process.h"
#include "sim_processor.h"
#include "sim_computer.h"
#include "sim_cpu_engine.h"


#endif
