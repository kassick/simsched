/*
 * ===========================================================================
 *
 *       Filename:  sim_scheduler.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  27-05-2015 09:28:02
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */


#ifndef __SIM_SCHEDULER_H__
#define __SIM_SCHEDULER_H__ 1

#include "sim_process.h"
#include "sim_processor.h"

// Função que adiciona uma um novo processo para ser escalonado
typedef void (*enqueue_fun_t)(void * priv, struct process * p);

// FUnção que remove um processo finalizado do escalonador
typedef void (*dequeue_fun_t)(void * priv, struct process * p);

// Função que avisa o escalonador que um processo acordou
typedef void (*wakeup_process_fun_t)(void * priv, struct process *p);

// Função que avisa o escalonador que o processo bloqueou
typedef void (*block_process_fun_t)(void * priv, struct process * p);

// Uma função de escalonamento -- seleciona um processo para executar
typedef struct process * (*sched_fun_t)(void * priv, struct processor * cpu);

// Função de Dispatch -- entrega a CPU a um processo
typedef void (*dispatch_fun_t)(void * priv, struct processor * cpu, struct process * proc);

// "Tick" de escalonamento: Seleciona 
typedef void (*sched_tick_fun_t)(void * priv, struct processor * cpu);

typedef struct scheduler * (*create_scheduler_fun_t)(struct computer * c, int argc, char ** argv);

typedef void (*terminate_sched_fun_t)(void * priv);


typedef void (*print_help_fun_t)();

struct scheduler {
    enqueue_fun_t enqueue;
    dequeue_fun_t dequeue;
    //switch_to_fun_t context_switch;
    wakeup_process_fun_t wake_up;
    block_process_fun_t block;
    sched_fun_t sched;
    dispatch_fun_t dispatch;
    sched_tick_fun_t tick;
    terminate_sched_fun_t terminate;
    void * priv;
};

#endif
