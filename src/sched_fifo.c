/*
 * ===========================================================================
 *
 *       Filename:  sched_fifo.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  16-05-2015 17:42:30
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "list.h"
#include "sim.h"
#include "sim_cpu_engine.h"
#include "sched_fifo.h"
#include "sim_common_events.h"



/*---------------------------------------------------------------------------
 *  FIFO Scheduler
 *  This is a first-in-first-out scheduler. The schedulling decition is always
 *  the first process in the ready list. The timeslice given is as much as the
 *  process needs. It's a non-preemptive scheduler (i.e. forking and other
 *  processes waking up do not cause it to schedule any other process then the
 *  current
 *  
 *  It works with multi-cpu as follows: Any time a new process is enqueued or
 *  awaken, it forces a tick() in the first idle CPU. A CPU will only be idle
 *  if there are no READY process in the list. When a new process is added,
 *  the forced tick() will have the idle CPU selectin this new process
 *  
 *  The list will have process in the state RUNNING and READY, but never
 *  BLOCKED -- i.e. block() will remove a process from the list the wakeup()
 *  will add it to the end of the list
 *  
 *--------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
 *  struct fifo_priv
 *  Description: This struct (pointed by scheduler->priv) holds the list of
 *  processes, shared by all CPUs
 *--------------------------------------------------------------------------*/
struct fifo_priv {
    struct list_head process_list;
    struct computer * computer;
};


/*---------------------------------------------------------------------------
 *  struct fifo_cpu_priv
 *  Description: Each CPU holds an instance of this struct, used to control
 *  the tick timing
 *--------------------------------------------------------------------------*/
struct fifo_cpu_priv {
    sim_time_t last_tick;
};

/*---------------------------------------------------------------------------
 *  Function: fifo_cpu_priv
 *  Description: Returns the fifo_cpu_priv struct for the CPU
 *  Param cpu: The CPU whose fifo_cpu_priv we need
 *  Return: The fifo_cpu_priv struct for the CPU
 *--------------------------------------------------------------------------*/
static struct fifo_cpu_priv * get_cpu_priv(struct processor * cpu) {
    return (struct fifo_cpu_priv*)cpu->sched_info;
}


/*---------------------------------------------------------------------------
 *  struct fifo_proc_holder
 *  Description: This is a struct associated with every process which is still
 *  alive (READY, RUNNING or BLOCKED). It allows us to have the process in a
 *  list within the fifo_priv struct.
 *--------------------------------------------------------------------------*/
struct fifo_proc_holder {
    struct list_head to_list;
    struct process * proc;
};

// Fifo tick function header, used by enqueue, wakeup
void fifo_tick(void * priv, struct processor * cpu);



/*---------------------------------------------------------------------------
 *  Function: fifo_wakeup_processor
 *  Description: Wakes up a processor that was idle
 *
 *  This function adds a reschedule event to the processor event queue. It
 *  assumes the processor was idle -- i.e. there was nothing scheduled for it
 *  at the current time
 *
 *  Param fp: Fifo priv struct
 *  Param cpu: CPU to wake up
 *
 *  Yields events:
 *      - evt_reschedule at cpu
 *--------------------------------------------------------------------------*/
void fifo_wakeup_processor(struct processor *cpu) {
    event_t *evt = sim_new_event();
    if (!evt) {
        fprintf(stderr, "FIFO Error -- could not allocate new event for reschedule\n");
        exit(1);
    }

    fprintf(stderr, "\t[FIFO Wake Up Processor %d]\n", cpu->cpu_id);

    evt->ts = sim_current_time();
    evt->handle = evt_reschedule;
    evt->priv = NULL;
    evt->destroy = NULL;

    if (!list_empty(&cpu->event_list) &&
        list_first_entry(&cpu->event_list, event_t, to_list)->ts <= sim_current_time())
    {
        fprintf(stderr, "FIFO WARNING: "
                        "Adding a reschedule event for NOW but there is "
                        "something pending there\n");
    }

    list_add(&evt->to_list, &cpu->event_list);
}



/*---------------------------------------------------------------------------
 *  Function: fifo_enqueue
 *  Description: Adds a newly created process to the scheduler. THis function
 *  creates a new fifo_proc_holder struct and stores it in the process
 *  sched_priv pointer. It also forces a tick in any idle CPU
 *--------------------------------------------------------------------------*/
void fifo_enqueue(void * priv, struct process *p)
{
    int i;
    struct fifo_priv *fpriv = (struct fifo_priv*)priv;
    struct fifo_proc_holder *fh = malloc(sizeof(struct fifo_proc_holder));
    if (!fh) {
        fprintf(stderr, "ERROR: Could not allocate fifo proc holder: ENOMEM\n");
        exit(1);
    }
    
    fprintf(stderr, "\t[FIFO ENQUEUE] at %lld pid %d\n", sim_current_time(), p->pid);

    fh->proc = p;
    p->sched_priv_info = fh;

    list_add_tail(&fh->to_list, &fpriv->process_list);

    // Multi-CPU fifo: Everytime a new process is enqueued, it ticks the first
    // available CPU
    for (i = 0; i < fpriv->computer->n_processors; i++) {
        if (!fpriv->computer->processors[i].current) {
            fifo_wakeup_processor(&fpriv->computer->processors[i]);
            break;
        }
    }
}


/*---------------------------------------------------------------------------
 *  Function: fifo_dequeue
 *  Description: Removes a process from the scheduler, destroy it's
 *  fifo_proc_holder struct
 *--------------------------------------------------------------------------*/
// Removes a process from the queue and releases all it's related structs
void fifo_dequeue(void * priv, struct process * p) {
    struct fifo_proc_holder * p_fph = NULL;
    struct fifo_priv *fp = (struct fifo_priv*)priv;

    if (!p->sched_priv_info) {
        fprintf(stderr, "FIFO IMPL ERROR: Process not found in queue\n");
        exit(1);
    }

    fprintf(stderr, "\t[FIFO dequeue] at %lld pid %d\n", sim_current_time(), p->pid);

    p_fph = (struct fifo_proc_holder*)p->sched_priv_info;

    list_del_init(&p_fph->to_list);
    free(p_fph);

    p->sched_priv_info = NULL;
}



/*---------------------------------------------------------------------------
 *  Function: fifo_wakeup_process
 *  Description: Wakes up a process, adds it to the end of the list. Also
 *  forces a tick in any idle processor
 *--------------------------------------------------------------------------*/
void fifo_wakeup_process(void * priv, struct process *p)
{
    struct fifo_priv * fp = priv;
    struct fifo_proc_holder *fph;
    int i;

    if (!p->sched_priv_info) {
        fprintf(stderr, "ERROR: Process %d was woke up with no sched priv info\n", p->pid);
        exit(1);
    }

    p->sigma_timeslice = 0;
    fprintf(stderr, "\t[FIFO wakeup] at %lld pid %d\n", sim_current_time(), p->pid);

    fph = (struct fifo_proc_holder*)p->sched_priv_info;
    list_add_tail(&fph->to_list, &fp->process_list);

    // Multi-CPU fifo: Everytime a new process is enqueued, it ticks the first
    // available CPU
    for (i = 0; i < fp->computer->n_processors; i++) {
        if (!fp->computer->processors[i].current) {
            fifo_wakeup_processor(&fp->computer->processors[i]);
            break;
        }
    }
}


/*---------------------------------------------------------------------------
 *  Function: fifo_block_process
 *  Description: Blocks a process -- i.e. removes it from the list
 *--------------------------------------------------------------------------*/
void fifo_block_process(void * priv, struct process * p)
{
    struct fifo_proc_holder *fph;
    
    if (!p->sched_priv_info) {
        fprintf(stderr, "ERROR: Process %d was blocked with no sched priv info\n", p->pid);
        exit(1);
    }

    fprintf(stderr, "\t[FIFO block] at %lld pid %d\n", sim_current_time(), p->pid);
    fph = (struct fifo_proc_holder*)p->sched_priv_info;

    // Remove a process from the list and mark it as blocked
    list_del_init(&fph->to_list);
}

/*---------------------------------------------------------------------------
 *  Function: fifo_tick
 *  Description: tick function for the fifo scheduler. Updates the timeslice
 *  of the running process. If the timeslice is 0, selects a new process and
 *  dispatches it to the CPU
 *  May select a NULL process (in this case, the CPU goes idle)
 *--------------------------------------------------------------------------*/
void fifo_tick(void * priv, struct processor * cpu) {
    struct fifo_priv * fpriv = (struct fifo_priv*)priv;
    struct process * p;

    fprintf(stderr, "\t[FIFO TICK] at %lld\n", sim_current_time());
    if (!cpu->scheduler) {
        fprintf(stderr, "ERROR: Scheduler for CPU %d not set\n", cpu->cpu_id);
        exit(0);
    }

    get_cpu_priv(cpu)->last_tick = sim_current_time();
    
    if (cpu->current) {
        // have a process currently holding the CPU
        // needs to update it's timeslice
        cpu->current->sigma_timeslice += (sim_current_time() - cpu->current->last_activated);
        cpu->current->timeslice -= (sim_current_time() - cpu->current->last_activated);

        // Current proces does not have time left, it will be preempted 
        if (cpu->current->status == RUNNING) {
            cpu->current->status = READY;
            // If it still has a timeslice, dispatch current
            if (cpu->current->timeslice > 0) {
                cpu->scheduler->dispatch(priv, cpu, cpu->current);
                return;
            }
        }
    }

    // Selects a process and gives the CPU to it
    p = cpu->scheduler->sched(cpu->scheduler->priv, cpu);
    cpu->scheduler->dispatch(cpu->scheduler->priv, cpu, p);
}


/*---------------------------------------------------------------------------
 *  Function: fifo_sched
 *  Description: Fifo scheduler: Always selects the first READY process in the
 *  list
 *--------------------------------------------------------------------------*/
struct process * fifo_sched(void * priv, struct processor * cpu)
{
    struct fifo_priv* fp = (struct fifo_priv*)priv;
    struct process * p = NULL;
    struct fifo_proc_holder *fph;
    
    fprintf(stderr, "\t[FIFO sched] at %lld for cpu %d\n", sim_current_time(), cpu->cpu_id);

    // Escolha: o primeiro READY da fila
    if (!list_empty(&fp->process_list)) {
        struct list_head * e;
        p = NULL;
        list_for_each(e, &(fp->process_list)) {
            fph = list_entry(e, struct fifo_proc_holder, to_list);
            if (fph->proc->status == READY)
            {
                p = fph->proc;
                break;
            }
        }
    }
   
    if (!p)
        fprintf(stderr, "\t[FIFO sched] at %lld going idle on cpu %d\n", sim_current_time(), cpu->cpu_id);
    return p;
}


/*---------------------------------------------------------------------------
 *  Function: fifo_dispatch
 *  Description: Dispatches a process in a CPU
 *--------------------------------------------------------------------------*/

void fifo_dispatch(void * priv, struct processor * cpu, struct process * p)
{
    struct fifo_priv *fp = priv;
    struct list_head *e;
    struct fifo_proc_holder * p_fph = NULL;


    fprintf(stderr, "\t[FIFO dispatch] at %lld to process %d\n", 
            sim_current_time(), 
            p? p->pid : -1);

    if (p) {
        p->timeslice = INT_MAX;
        p->last_activated = sim_current_time();
        p->status = RUNNING;
    }

    cpu->current = p;
}



/*---------------------------------------------------------------------------
 *  Function: fifo_create_scheduler
 *  Description: This is the only function exported outside this file. It's
 *  used in sim_main.c to create a FIFO scheduler when the user requires it.
 *  
 *  This function creates a scheduler struct with the pointers set up to the
 *  fifo_* functions (enqueue, dequeue, wakeup, block, tick, sched, dispatch)
 *  The priv field is set to a newly allocated instance of fifo_priv (the
 *  list). Each processor inside computer has it's scheduler set to this newly
 *  created sched and it's sched_priv_info set to an instance of fifo_cpu_priv
 *
 *  Param c: The computer
 *  Return: A newly created scheduler struct
 *--------------------------------------------------------------------------*/
struct scheduler * fifo_create_scheduler(struct computer * c, int argc, char**argv)
{
    int i;
    struct scheduler * sched = malloc(sizeof(struct scheduler));
    if (!sched)
        return NULL;

    struct fifo_priv *priv = malloc(sizeof(struct fifo_priv));
    if (!priv) {
        free(sched);
        return NULL;
    }

    // For this simulated computer
    priv->computer = c;

    // initialize list as empty
    priv->process_list.next =
        priv->process_list.prev =
        &(priv->process_list);

    sched->priv = priv;
    sched->enqueue = fifo_enqueue;
    sched->dequeue = fifo_dequeue;
    sched->block = fifo_block_process;
    sched->wake_up = fifo_wakeup_process;
    sched->sched = fifo_sched;
    sched->dispatch = fifo_dispatch;
    sched->tick = fifo_tick;
    sched->terminate = NULL;

    for (i = 0; i < c->n_processors; i++) {
        c->processors[i].scheduler = sched;
        c->processors[i].sched_info = malloc(sizeof(struct fifo_cpu_priv));
        get_cpu_priv(&c->processors[i])->last_tick = 0;
    }
    return sched;
}
