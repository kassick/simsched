/*
 * ===========================================================================
 *
 *       Filename:  sched_dummy.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  31-05-2015 19:59:34
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#ifndef __DUMMY_H__
#define __DUMMY_H__ 1

#include "sim_computer.h"
struct scheduler * dummy_create_scheduler(struct computer * c, int argc, char**argv);
void dummy_print_help();

#endif
