/*
 * ===========================================================================
 *
 *       Filename:  sim_computer.h
 *
 *    Description:  Simulated computer
 *
 *        Version:  1.0
 *        Created:  27-05-2015 09:22:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIM_COMPUTER_H__
#define __SIM_COMPUTER_H__ 1

#include "sim_processor.h"
#include "sim_process.h"

struct computer {
    struct processor * processors;
    int n_processors;
    struct process * processes;
    int n_processes;

    struct scheduler * scheduler;

    int cur_cpu;
};
#endif
