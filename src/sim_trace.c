/*
 * ===========================================================================
 *
 *       Filename:  simtrace.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  21-05-2015 20:39:57
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "sim_trace.h"
#include "sim_process.h"
#include "sim_processor.h"


// Trace Format:
// First Line:
//  n
// where n is the number of processes
// Next: n lines, each
//  pid parent_pid n_parent_burst prio n_cpu_bursts n_io_bursts
// Followed by n_cpu_bursts lines
//  cpu_burst io_burst
// The last line will be
//  cpu_burst -1
// Meaning there is no I/O burst after the last cpu burst


/*---------------------------------------------------------------------------
 *  Function: parse_trace
 *  Description: Reads a trace file and returns an allocated array of
 *  processes
 *
 *  Trace format is:
 *  <nprocs>
 *  <pid0> <parent_pid0> <parent_burst0> <after0> <prio0> <n_cpu_bursts0> <n_io_bursts0>
 *  <cpu_burst_0_len> <io_burst_0_len>
 *  <cpu_burst_1_len> <io_burst_1_len>
 *  ...
 *  <cpu_burst<n_cpu_bursts0 - 1>_len> -1
 *  <pid1> <parent_pid1> <parent_burst1> <after1> <prio1> <n_cpu_bursts1> <n_io_bursts1>
 *  <cpu_burst_0_len> <io_burst_0_len>
 *  <cpu_burst_1_len> <io_burst_1_len>
 *  ...
 *  <cpu_burst<n_cpu_bursts1 - 1>_len> -1
 *  ...
 *  <pid<nprocs - 1>> ...
 *--------------------------------------------------------------------------*/
struct process * parse_trace(const char * fname,
                                        int * n_procs) {
    FILE * fh = fopen(fname, "r+");
    struct process * processes = NULL;
    int i, j;
    int line;

    printf("%s\n", fname);
    fh = fopen(fname, "r+");
    if (!fh)
        return NULL;

    if ( fscanf(fh, "%d", n_procs) != 1 ) {
        fprintf(stderr, "Could not parse %s: Could not read number of processes\n",
                        fname);
        goto early_ret;
    }

    if (*n_procs < 1) {
        fprintf(stderr, "Could not parse %s: Expected more then 1 process, but got %d\n",
                        fname,
                        *n_procs);
        goto early_ret;
    }

    processes = malloc( (*n_procs) * sizeof(struct process));
    if (processes == NULL) {
        fprintf(stderr, "Could not parse %s: ENOMEM\n",
                        fname);
        exit(1);
    }

    bzero(processes, (*n_procs) * sizeof(struct process));

    line = 2;
    for (i = 0; i < (*n_procs); i++, line++) {
        processes[i].status = UNITIALIZED;
        processes[i].children_created = 0;
        processes[i].last_activated = 0;
        processes[i].last_ready = 0;
        processes[i].last_cpu = -1;
        processes[i].timeslice = 0;
        processes[i].sigma_timeslice = 0;
        processes[i].sched_priv_info = NULL;
        processes[i].stats.sigma_waiting = 0;
        processes[i].stats.migrations = 0;
        processes[i].cpu_bursts =
            processes[i].io_bursts = NULL;
        //  pid parent_pid n_parent_burst n_cpu_bursts n_io_bursts
        int n_entries = fscanf(fh,  "%d " // pid
                                    "%d " // parent_pid
                                    "%d " // n_parent_burst
                                    "%d " // after
                                    "%d " // prio
                                    "%d " // n_cpu_bursts
                                    "%d", // n_io_bursts
                                    &processes[i].pid,
                                    &processes[i].parent_pid,
                                    &processes[i].created_on_parent_burst,
                                    &processes[i].after,
                                    &processes[i].prio,
                                    &processes[i].n_cpu_bursts,
                                    &processes[i].n_io_bursts);
        if (n_entries != 7) {
            fprintf(stderr, "Could not parse file %s: Wrong format on line %d\n",
                            fname, line);
            exit(1);
        }

        if ( (processes[i].n_cpu_bursts > 1) &&
              processes[i].n_io_bursts != (processes[i].n_cpu_bursts - 1) )
        {
            fprintf(stderr, "Could not parse file %s: "
                            "Line %d has incompatible number of I/O and "
                            "CPU bursts\n", fname, line);
            exit(1);
        }

        processes[i].cpu_bursts = malloc(sizeof(int) * processes[i].n_cpu_bursts);
        processes[i].io_bursts  = malloc(sizeof(int) * processes[i].n_cpu_bursts);

        if (processes[i].cpu_bursts == NULL || processes[i].io_bursts == NULL) {
            fprintf(stderr, "Could not parse %s: ENOMEM\n",
                            fname);
            exit(1);
        }

        for (j = 0; j < processes[i].n_cpu_bursts; j++, line++) {
            n_entries = 
                fscanf(fh, "%d %d", &processes[i].cpu_bursts[j],
                                    &processes[i].io_bursts[j]);
            if (n_entries != 2) {
                fprintf(stderr, "Could not parse %s: Invalid format in line %d\n",
                                fname, line);
            }
        }

        processes[i].io_bursts[processes[i].n_cpu_bursts - 1] = -1;
    }

    fclose(fh);
    return processes;

    // Error management
early_free_ret:
    while (i--) {
            if (processes[i].cpu_bursts)
                free(processes[i].cpu_bursts);
            if (processes[i].io_bursts)
                free(processes[i].io_bursts);
    }
    free(processes);
    processes = NULL;
early_ret:
    fclose(fh);
    return processes;
}


void dump_trace(struct process* processes, int n_procs) {
    int i;
    int j;

    printf("%d\n", n_procs);
    for (i = 0; i < n_procs; i++) {
        printf("%d %d %d %d %d %d %d\n",
               processes[i].pid,
               processes[i].parent_pid,
               processes[i].created_on_parent_burst,
               processes[i].after,
               processes[i].prio,
               processes[i].n_cpu_bursts,
               processes[i].n_io_bursts);
        for (j = 0; j < processes[i].n_cpu_bursts; j++) {
            printf("%d %d\n", processes[i].cpu_bursts[j], processes[i].io_bursts[j]);
        }
    }
}


void dump_affinity(struct processor* processors, int nprocs);
/*---------------------------------------------------------------------------
 *  Function: parse_affinity
 *  Description: Reads an affinity file and initialize a computer's CPUs.
 *
 *  The affinity file has the format:
 *
 *  <n_cpus_file>
 *  <affinity_cpu_0_to_0>
 *  <affinity_cpu_0_to_1>
 *  ...
 *  <affinity_cpu_0_to_<n_cpus_file-1>>
 *  <affinity_cpu_1_to_0>
 *  <affinity_cpu_1_to_1>
 *  ...
 *  <affinity_cpu_1_to_<n_cpus_file-1>>
 *  ...
 *  <affinity_cpu_<n_cpus_file-1>_to_<n_cpus_file-1>>
 *  
 *  This means, for 4 CPUs, it expects 16 lines of affinity.
 *
 *  Affinity may be assymetric (i.e. affinity from 0 to 1 == 1, from 1 to 0 ==
 *  10 , if this makes sense at all)
 *
 * The user may select to run only 2 CPUs of a 16 CPUs affinity file. In this
 * case, it will only consider the affinity for the first 2 CPUs
 *--------------------------------------------------------------------------*/
int parse_affinity(struct computer * c, int n_cpus, const char * const fname) {
    int n_cpus_file;
    int i, j;
    FILE * fh;
    
    if (fname == NULL) {
        fprintf(stderr, "Not loading an affinity file\n");
        c->processors = malloc(n_cpus * sizeof(struct processor));
        if (!c->processors)
        {
            fprintf(stderr, "ERROR creating processors, ENOMEM\n");
            exit(1);
        }

        c->n_processors = n_cpus;

        for (i = 0; i < n_cpus; i++) {
            init_cpu(&c->processors[i], i, c);
            c->processors[i].affinity = malloc(n_cpus * sizeof(int));
            if (!c->processors[i].affinity) {
                fprintf(stderr, "ERROR allocating affinity for proc %d: ENOMEM\n", i);
                exit(1);
            }

            // Equal affinity for everyone
            for (j = 0; j < n_cpus; j++) {
                c->processors[i].affinity[j] = 1;
            }
        }

        dump_affinity(c->processors, c->n_processors);
        return 0;
    }

    fh = fopen(fname, "r+");
    if (!fh) {
        fprintf(stderr, "Could not open affinity file %s: %s\n", fname, strerror(errno));
        return 1;
    }

    if (fscanf(fh, "%d", &n_cpus_file) != 1) {
        fprintf(stderr, "Could not parse %s: invalid header\n", fname);
        fclose(fh);
        return 1;
    }

    if ((n_cpus > 0) && (n_cpus > n_cpus_file)) {
        fprintf(stderr, "Could not parse %s: Simulation requires %d CPUs"
                        "but affinity defined for %d CPUs\n", 
                        fname, n_cpus, n_cpus_file);
        fclose(fh);
        return 1;
    }

    if (n_cpus && (n_cpus < n_cpus_file))
        c->n_processors = n_cpus;
    else
        c->n_processors = n_cpus_file;

    c->processors = malloc(c->n_processors * sizeof(struct processor));
    if (!c->processors) {
        fprintf(stderr, "Could not parse %s: ENOMEM allocating %d processors\n", fname, c->n_processors);
        fclose(fh);
        return 1;
    }

    j = 0;
    for (i = 0; i < n_cpus_file; i++) {
        int nread = 0;

        if (i < c->n_processors) {
            struct processor *cpu = &c->processors[i];
            init_cpu(cpu, i, c);
            cpu->affinity = malloc(c->n_processors * sizeof(int));
            if (!cpu->affinity)
                goto early_ret_free;


            for (j = 0; j < c->n_processors; j++, nread++) {
                if (fscanf(fh, "%d", &cpu->affinity[j]) != 1) {
                    fprintf(stderr, "Could not parse %s: early EOF?\n", fname);
                    goto early_ret_free;
                }
            }
        }

        for (; j < n_cpus_file; j++) {
            if (fscanf(fh, "%d", &nread) != 1) {
                fprintf(stderr, "Could not parse %s: Early EOF?\n", fname);
                goto early_ret_free;
            }
        }
    }

    fclose(fh);


    fprintf(stderr, "Running with %d processors, affinity is:\n", c->n_processors);
    dump_affinity(c->processors, c->n_processors);
    return 0;

early_ret_free:
    while (i--) {
        if (c->processors[i].affinity)
            free(c->processors[i].affinity);
    }
    free(c->processors);
early_ret:
    fclose(fh);
    return 1;
}

void dump_affinity(struct processor* processors, int nprocs) {
    int i,j;

    fprintf(stderr, "Affinity :\n");
    for (i = 0; i < nprocs; i++) {
        for (j = 0; j < nprocs; j++) {
            fprintf(stderr, "%d-%d = %d\n", i, j, processors[i].affinity[j]);
        }
    }
}
