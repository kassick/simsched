/*
 * ===========================================================================
 *
 *       Filename:  sim_processor.h
 *
 *    Description:  Simulated processor
 *
 *        Version:  1.0
 *        Created:  27-05-2015 09:24:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIM_PROCESSOR_H__
#define __SIM_PROCESSOR_H__ 1

#include "sim_computer.h"
#include "sim_cpu_engine.h"
#include "list.h"

struct processor {
    int cpu_id;
    int * affinity;
    struct computer * computer;
    struct scheduler * scheduler;
    void * sched_info;
    struct process * current;

    // This is simulation specific
    struct list_head event_list;
    sim_time_t ocupied_time,
               idle_time;
};

static inline void init_cpu(struct processor * cpu, int id, struct computer * c) {
    cpu->cpu_id = id;
    cpu->computer = c;
    cpu->affinity = NULL;
    cpu->sched_info = NULL;
    cpu->scheduler = NULL;
    cpu->current = NULL;
    cpu->idle_time = cpu->ocupied_time = 0; 
    cpu->event_list.prev = cpu->event_list.next = &(cpu->event_list);
}

#endif
