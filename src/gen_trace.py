#!/usr/bin/env python
# -*- coding: utf-8 -*-

# File: "gen_trace.py"
# Created: "Sex, 22 Mai 2015 13:58:41 -0300 (kassick)"
# Updated: "Dom, 31 Mai 2015 17:47:49 -0300 (kassick)"
# $Id$
# Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br>

import sys
import os
import numpy.random as nprandom
import random
import numpy as np

# Tests configurations:
# Each test is tuple :
#   (nprocs, prio_dist, #cpu-burst-dist , cpu-burst-len-dist, io-burst-len-dist)
tests = [   (   10,                     # n nprocs
                "trace_10_procs.txt",
                ("normal",   (50, 3)),   # Prio (dist, dist_params)
                ("normal",   (2, 1)),    # Fan-out distribution
                ("sysdist",  (0, 5 )),   # Inter-fork interval in cpu bursts
                ("sysdist",  (1, 1000)), # #CPU-Bursts dist
                ("normal",   (50, 5)),   # CPU Burst len dist
                ("poisson",  (500,)),     # IO Burst len dist
            ),
            (   1000,
                "trace_1k_procs.txt",
                ("normal",  (50,20)),   # Prio: very spread prios
                ("normal",   (50, 5)),    # Fan-out distribution
                ("sysdist",  (2, 100)),   # Inter-fork interval in cpu bursts
                ("poisson", (400,)),      # #bursts
                ("sysdist", (20, 200)), # cpu burst len
                ("sysdist", (2000, 20000)), # io burst len
                ),
            ]

def meta_normal(loc, scale):
    """
    Random variables to the Normal distribution

    @param loc: float
    @desc loc: Mean / Centre
    @param scale: float
    @desc loc: Standard avg
    @rtype: function(int)
    @rdesc: Callable which yields n samples from the random generator
    """

    return lambda n: nprandom.normal(loc, scale, n)

def meta_poisson(lam):
    """
    Random variables to the poisson distribution

    @param lam: float
    @desc lam:  Expectation/Avg
    @rtype: function(int)
    @rdesc: Callable that yields n samples from the random generator
    """

    return lambda n: nprandom.poisson(lam, n)

def meta_power(a, m):
    """
    Random variables to the power distribution, normalidez to max
    @param a: float
    @desc a: Shape
    @param m: float
    @desc m: Maximum
    @rtype: function(int)
    @rdesc: Callable that yields n samples from the distribution
    """

    def __meta_power(n):
        tmp = nprandom.power(a, n)
        return tmp * m

    return __meta_power

def meta_pareto(a, m):
    """
    Random variables to the pareto distribution
    This function, as it is, is not a good option for most cases in this trace
    generator, as it gives too many float values in the interval [m, m+1]

    @param a: float
    @desc a: Shape / alpha
    @param m: float
    @desc m: Minimum
    @rtype: function(int)
    @rdesc: Callable that yields n random values according the the distribution
    """

    def __meta_pareto(n):
        tmp = nprandom.pareto(a, n)
        return tmp+m

    return __meta_pareto

def meta_sysdist(start, stop):
    """
    Random variables according to the system random (equiprobabilistic) engine
    @rtype: function(int)
    @rdesc: Function that yields n samples from the standard random engine
    """

    def __meta_sysdist(n):
        tmp = np.ndarray(shape=(n), dtype = float)
        for i in range(0, n):
            tmp[i] = random.randrange(start, stop)
        return tmp

    return __meta_sysdist

def meta_const(const):
    """
    Constant function, returns a callable that yields n times const
    @rtype: function(int)
    @rdesc Function that yields n samples from the const function x=const
    """

    def __meta_const(n):
        tmp = np.ndarray(shape=(n), dtype = float)
        for i in range(0, n):
            tmp[i] = const
        return tmp

    return __meta_const


DISTS_DICT = {"sysdist": meta_sysdist,
              "pareto": meta_pareto,
              "power": meta_power,
              "poisson": meta_poisson,
              "normal": meta_normal,
              "const": meta_const,
              }

PROC_PID       = 0
PROC_PRIO      = 1
PROC_PPID      = 2
PROC_PBURST    = 3
PROC_PAFTER    = 4
PROC_NCHILDREN = 5
PROC_NBURSTS   = 6
PROC_CPUBURSTS = 7
PROC_IOBURSTS  = 8
PROC_MAXELEM   = 9

def main():
    n_test = 0

    for (nprocs, fname, prio_dist, fanout_dist, interfork_dist, nbursts_dist, cpu_burst_dist, io_burst_dist) in tests:

        print >>sys.stderr, \
            "Test #%d: %d procs, fanout: %s, interfork: %s, n_bursts: %s, cpu_bursts: %s, io_bursts: %s" % \
            (n_test, nprocs, fanout_dist[0], interfork_dist[0], nbursts_dist[0], cpu_burst_dist[0], io_burst_dist[0])

        if not(fname):
            fname = "trace_%d.txt" % n_test
        procs = []
        parents = {}
        next_pid = 0

        prio_rand_fun      = apply(DISTS_DICT[prio_dist[0]],  prio_dist[1])
        fanout_rand_fun    = apply(DISTS_DICT[fanout_dist[0]],  fanout_dist[1])
        interfork_rand_fun = apply(DISTS_DICT[interfork_dist[0]],  interfork_dist[1])
        nbursts_rand_fun   = apply(DISTS_DICT[nbursts_dist[0]],  nbursts_dist[1])
        cpu_burst_rand_fun = apply(DISTS_DICT[cpu_burst_dist[0]], cpu_burst_dist[1])
        io_burst_rand_fun  = apply(DISTS_DICT[io_burst_dist[0]], io_burst_dist[1])

        # Generate the processes and cpu and io burst, prio etc
        for pid in range(0, nprocs):
            prio = int(prio_rand_fun(1)[0])
            n_bursts = int(nbursts_rand_fun(1)[0])
            cpu_bursts = cpu_burst_rand_fun(n_bursts)
            io_bursts = io_burst_rand_fun(n_bursts)
            io_bursts[n_bursts-1] = -1

            proc = [None] * PROC_MAXELEM
            proc[PROC_PID] = pid
            proc[PROC_PRIO] = prio
            proc[PROC_PPID] = 0
            proc[PROC_PBURST] = 0
            proc[PROC_PAFTER] = 0
            proc[PROC_NCHILDREN] = 0
            proc[PROC_NBURSTS] = n_bursts
            proc[PROC_CPUBURSTS] = cpu_bursts
            proc[PROC_IOBURSTS] = io_bursts
            procs.append(proc)

        # Now parent everyone
        for pid in range(0, nprocs):
            # Want this number of children
            n_childs = -1
            while (n_childs < 0):
                n_childs = int(fanout_rand_fun(1)[0])
            children = []

            # print >> sys.stderr, "Process %d wants %d children" % (pid, n_childs)
            # Tries to find at most n_childs orphan pids
            # Can find less then that -- say a high fanout with fes process will
            # have the first ones with all the childs
            last_child_offset = 0
            for nchild in range(0, n_childs):
                child_pid = -1
                for next_child in range(pid + last_child_offset+1, nprocs):
                    if not (next_child in parents):
                        child_pid = next_child
                        break

                if (child_pid != -1): #Found !
                    parents[child_pid] = pid
                    children.append(child_pid)
                else:
                    break

                last_child_offset = next_child - pid
            pass

            procs[pid][PROC_NCHILDREN] = len(children)

            # Now spread the creation of the children
            #
            interfork = interfork_rand_fun(len(children))
            print interfork

            # makes sure we won't be forking past the last burst
            last_fork = 0
            for ifork in range(0, len(children)):
                cur_burst_fork = last_fork + interfork[ifork]
                if (cur_burst_fork) > (procs[pid][PROC_NBURSTS] - 1):
                    print >>sys.stderr, "Forking after last burst", cur_burst_fork, interfork[ifork], last_fork
                    cur_burst_fork = procs[pid][PROC_NBURSTS] - 1 # happens in the last burst...

                cur_burst_len = procs[pid][PROC_CPUBURSTS][cur_burst_fork]
                print "Forking %d from %d in %dth butst" % (children[ifork],
                                                            pid,
                                                                cur_burst_fork)

                child = procs[children[ifork]]
                child[PROC_PPID] = pid
                child[PROC_PBURST] = cur_burst_fork
                child[PROC_PAFTER] = nprandom.random_integers(0, cur_burst_len-1)

                last_fork = cur_burst_fork
            pass

            if len(children) == 0:
                print >>sys.stderr, "No more parenting to do after PID %d" % pid
                break

        # Now make sure that we do not have orphans
        last_non_orphan = 0
        for pid in range(1, nprocs):
            if pid in parents:
                last_non_orphan = pid
            else:
                break

        if (last_non_orphan < (nprocs - 1)):
            # OOps, got orphan
            print >>sys.stderr, "Warning: Orphaned processes going to be fathered by %d" % last_non_orphan

            n_orphans = 0
            after = 1
            nbursts = procs[last_non_orphan][PROC_NBURSTS]
            for orphan_pid in range(pid, nprocs):
                if not (orphan_pid in parents):
                    parents[orphan_pid] = last_non_orphan
                    child = procs[orphan_pid]
                    child[PROC_PPID] = last_non_orphan
                    child[PROC_PBURST] = nbursts - 1
                    child[PROC_PAFTER] = after
                    after = after + 1
                    procs[last_non_orphan][PROC_NCHILDREN] = \
                        procs[last_non_orphan][PROC_NCHILDREN] + 1
                    n_orphans = n_orphans + 1

            # Adjust burst length to make sure we have time to create all the
            # orphans
            if procs[last_non_orphan][PROC_CPUBURSTS][nbursts-1] < after:
                procs[last_non_orphan][PROC_CPUBURSTS][nbursts-1] = after + 1

            print >>sys.stderr, "%d processes adopted" % n_orphans

        # Now sanity check
        for pid in range(1, nprocs):
            if not(pid in parents):
                print >>sys.stderr, "ERROR: PID %d is orphan" % pid
                sys.exit(1)

        sigma_child = sum( (( p[PROC_NCHILDREN] for p in procs )) )
        if (sigma_child != nprocs-1):
            print >>sys.stderr, "ERROR: Got %d children for %d procs" % (sigma_child, nprocs)
            sys.exit(1)

        for p in procs:
            parent_pid = p[PROC_PPID]
            parent_proc = procs[parent_pid]
            if (p[PROC_PAFTER]) > parent_proc[PROC_CPUBURSTS][p[PROC_PBURST]]:
                print >>sys.stderr, "ERROR: PID %d created after it's parent's burst finished" % p[PROC_PID]
                sys.exit(1)


        fh = file(fname, "w+")
        if not(fh):
            print >> sys.stderr, "ERROR: Could not open output", fname
            sys.exit(1)

        print >>fh, "%d" % nprocs
        for p in procs:
            print >>fh, "%d %d %d %d %d %d %d" % (p[PROC_PID],
                                                  p[PROC_PPID],
                                                  p[PROC_PBURST],
                                                  p[PROC_PAFTER],
                                                  p[PROC_PRIO],
                                                  p[PROC_NBURSTS],
                                                  p[PROC_NBURSTS] -1)
            for (c, i) in zip (p[PROC_CPUBURSTS], p[PROC_IOBURSTS]):
                print >>fh, "%d %d" % (c, i)

        fh.close()

        n_test = n_test + 1


if __name__ == "__main__":
    main()
