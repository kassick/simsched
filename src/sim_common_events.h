/*
 * ===========================================================================
 *
 *       Filename:  sim_common_events.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  24-05-2015 01:04:33
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIM_COMMON_EVENTS_H__
#define __SIM_COMMON_EVENTS_H__ 1

#include "sim.h"
void evt_reschedule(struct processor *cpu, void * priv);
void evt_wakeup_process(struct processor *cpu, void *priv);
void evt_block_process(struct processor * cpu, void * priv);
void evt_fork_process(struct processor *cpu, void * priv);
void evt_exit_processes(struct processor *cpu, void * priv);
const char * const name_common_evt(void * f);

#endif
