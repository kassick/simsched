/*
 * ===========================================================================
 *
 *       Filename:  sched_dummy.c
 *
 *    Description:  DUMMY Does nothing!
 *
 *        Version:  1.0
 *        Created:  31-05-2015 19:47:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>

#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include "list.h"
#include "sim.h"
#include "sim_cpu_engine.h"
#include "sched_rr.h"
#include "sim_common_events.h"

#include "sched_dummy.h"



struct dummy_priv {
    int nothing;
};

struct dummy_cpu_priv {
    int nothing;
};

static struct dummy_cpu_priv * get_cpu_priv(struct processor * cpu) {
    return (struct dummy_cpu_priv*)cpu->sched_info;
}

void dummy_enqueue(void * priv, struct process *p)
{
    fprintf(stderr, "DUMMY NOTHING\n");
}


void dummy_dequeue(void * priv, struct process * p) {
    fprintf(stderr, "DUMMY NOTHING\n");
}



void dummy_wakeup_process(void * priv, struct process *p)
{
    fprintf(stderr, "DUMMY NOTHING\n");
}

void dummy_block_process(void * priv, struct process * p)
{
    fprintf(stderr, "DUMMY NOTHING\n");
}

void dummy_tick(void * priv, struct processor * cpu) {
    fprintf(stderr, "DUMMY NOTHING\n");
}

struct process * dummy_sched(void * priv, struct processor * cpu)
{
    fprintf(stderr, "DUMMY NOTHING\n");
    return NULL;
}


void dummy_dispatch(void * priv, struct processor * cpu, struct process * p)
{
    fprintf(stderr, "DUMMY NOTHING\n");
}


void dummy_print_help() {
    printf("\t--ignore-me <value>       : "
            "ignored parameter as DUMMY DOES NOTHING\n");
}

void dummy_parse_opts(struct dummy_priv *rpriv, int argc, char** argv)
{
    static struct option long_options[] =
        { {"ignore-me", required_argument,  NULL, 0},
          {NULL, 0, 0, 0},
        };

    optind = 1;

    while (1) {
        int longindex;
        int opt = getopt_long_only(argc, argv, "", long_options, &longindex);

        if (opt == -1)
            break;

        if (opt == 0 && !strcmp(long_options[longindex].name, "ignore-me"))
            rpriv->nothing = atol(optarg);
        else {
            fprintf(stderr, "Invalid options. Use --help\n");
            exit(1);
        }
    }
}

struct scheduler * dummy_create_scheduler(struct computer * c, int argc, char**argv)
{
    int i;
    struct scheduler * sched = malloc(sizeof(struct scheduler));
    if (!sched)
        return NULL;

    struct dummy_priv *priv = malloc(sizeof(struct dummy_priv));
    if (!priv) {
        free(sched);
        return NULL;
    }

    dummy_parse_opts(priv, argc, argv);

    sched->priv = priv;
    sched->enqueue = dummy_enqueue;
    sched->dequeue = dummy_dequeue;
    sched->block = dummy_block_process;
    sched->wake_up = dummy_wakeup_process;
    sched->sched = dummy_sched;
    sched->dispatch = dummy_dispatch;
    sched->tick = dummy_tick;
    sched->terminate = NULL;

    for (i = 0; i < c->n_processors; i++) {
        c->processors[i].scheduler = sched;
        c->processors[i].sched_info = malloc(sizeof(struct dummy_cpu_priv));
        get_cpu_priv(&c->processors[i])->nothing= 0;
    }

    fprintf(stderr, "Created DUMMY with ignored %d\n", priv->nothing);

    return sched;
}
