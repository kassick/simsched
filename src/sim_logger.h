/*
 * ===========================================================================
 *
 *       Filename:  sim_logger.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  22-06-2015 09:07:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIM_LOGGER_H__
#define __SIM_LOGGER_H__ 1

#include <stdio.h>

FILE * sim_log_open(const char * const fname);
void sim_log_resched(int cpu_id, int from_pid, int to_pid);
void sim_log_close();
#endif
