/*
 * ===========================================================================
 *
 *       Filename:  sim_common_events.c
 *
 *    Description:  Events that simulate syscalls on the OS
 *
 *        Version:  1.0
 *        Created:  24-05-2015 00:43:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include "sim.h"
#include "sim_common_events.h"
#include "sim_logger.h"

// Fork the process children for the current timeslice:
// Must happen ONCE per cpu burst
void fork_children(struct computer * c, struct processor * cpu, struct process * p);
void cancel_fork_children(struct computer * c, struct processor * cpu, struct process * p);

// Add the end of the cpu burst event
// Must happen once every tick
void add_cpu_bust_end_evt(struct processor *cpu, struct process *p);
void need_reschedule(struct processor *cpu);

/*---------------------------------------------------------------------------
 *  Function: evt_reschedule
 *  Description: Event Handler for a RESCHEDULE . Happens whenever a
 *  reschedule must happen. This function is called directly whenever
 *  some async event happens (i.e. a new process is ready to start or a
 *  process woke up from a blocked state) or via the event engine when the
 *  timeslice of the current process ended before blocking.
 *
 *  This function first calls sched->tick()  to have the scheduler update the
 *  timeslices, select a new process and call dispatch on it.
 *
 *  Then it creates the events to fork any children created during the first
 *  cpu burst (fork_children). These are **events**, so they may happen any
 *  time between current time and current time + timeslice.
 *
 *  It also must cancel any pending event of BLOCK, RESCHEDULE or FORK if the
 *  scheduler selected a new process to run. This is a mix of simulation-only
 *  requirement and real-life OS, as an operating system calling reschedule
 *  may cancel a timer that would call tick() sometime in the future not to
 *  interrupt a high priority job
 *  
 *  Last, it must create an event at the end of the current timeslice. Here
 *  it's simulation specific, as we must see what is the next event in the
 *  simulation and schedulle accordingly -- i.e. the process will block, the
 *  process will use all it's timeslice before blocking, the process will call
 *  exit(), ...
 *  
 *  Param cpu: The CPU in which the some event caused a reschedule
 *  Param priv: Priv data. Ignored in evt_reschedule, but used to tag the
 *  process that caused the reschedule -- thus we can use it to find events to
 *  be cancelled
 *  
 *  Yields events:
 *      - evt_reschedule: If the new current process finished it's timeslice
 *      before blocking
 *      - evt_block: If the new current process blocks before the end of it's
 *      timeslice
 *      - evt_fork_process: For any children created within the current
 *      timeslice
 *      - evt_exit_processes: If the new current process finished after
 *      completing it's timeslice
 *--------------------------------------------------------------------------*/
// Happens when we need to reschedule
void evt_reschedule(struct processor *cpu, void * priv) {
    struct process *prev_current = cpu->current;
    event_t *evt;

    fprintf(stderr, "[CPU %2d RESCHEDULE at %10lld] Current %d with (non up to date) timeslice %lld\n",
                    cpu->cpu_id,
                    sim_current_time(),
                    cpu->current? cpu->current->pid : -1,
                    cpu->current? cpu->current->timeslice : -1);

    // Adjust last ready BEFORE tick
    if (prev_current && prev_current->status == RUNNING) {
        prev_current->last_ready = sim_current_time();
    }

    cpu->scheduler->tick(cpu->scheduler->priv, cpu);

                    
    sim_log_resched(cpu->cpu_id, prev_current? prev_current->pid : -1,
                                 cpu->current? cpu->current->pid : -1);
                                
    fprintf(stderr, "\tReschedule %d -> %d, timeslice: %lld sigma_timeslice: %lld\n", 
                    prev_current? prev_current->pid : -1,
                    cpu->current? cpu->current->pid : -1,
                    cpu->current? cpu->current->timeslice : -1,
                    cpu->current? cpu->current->sigma_timeslice: -1);


    // Adjust stats AFTER tick
    if (cpu->current) {
        if (cpu->current->last_cpu != cpu->cpu_id) {
            cpu->current->stats.migrations++;
        }
        cpu->current->last_cpu = cpu->cpu_id;

        fprintf(stderr, "\t\tLast ready: %lld\n", cpu->current->last_ready);
        fprintf(stderr, "\t\tsigma ready: %lld\n", cpu->current->stats.sigma_waiting);

        cpu->current->stats.sigma_waiting += (sim_current_time() - cpu->current->last_ready);
        cpu->current->last_ready = sim_current_time();
    }

    // Prev lost CPU
    // Must cancel some events
    if (prev_current) {
        // Cancel any pending block, exit or reschedule created by the previous current
        // finds a BLOCK event for the prev current
        evt = find_pending_event(cpu->computer, 
                                 -1, // ts
                                 evt_block_process,
                                 prev_current,
                                 FIND_PENDING_BY_HANDLE | FIND_PENDING_BY_PRIV);
        if (evt) {
            sim_cancel_event(evt);
            fprintf(stderr, "\tCancelled block of process %d as it lost CPU\n", prev_current->pid);
        }
        
        // finds a EXIT event for the prev current
        evt = find_pending_event(cpu->computer, 
                                 -1, // ts
                                 evt_exit_processes,
                                 prev_current,
                                 FIND_PENDING_BY_HANDLE | FIND_PENDING_BY_PRIV);
        if (evt) {
            sim_cancel_event(evt);
            fprintf(stderr, "\tCancelled exit of process %d as it lost CPU\n", prev_current->pid);
        }

        // Finds a RESCHEDULE event for the previous current
        evt = find_pending_event(cpu->computer, 
                                -1, // ts
                                evt_reschedule,
                                prev_current,
                                FIND_PENDING_BY_HANDLE | FIND_PENDING_BY_PRIV);
        if (evt) {
            sim_cancel_event(evt);
            fprintf(stderr, "\tCancelled preempt of process %d as it lost CPU\n", prev_current->pid);
        }

        // Cancel any pending fork by prev_current
        cancel_fork_children(cpu->computer, cpu, prev_current);
    }

    if (cpu->current) {
        // Create the fork events for the children of current
        fork_children(cpu->computer, cpu, cpu->current);
        // Create the exit/block/reschedule events for current
        add_cpu_bust_end_evt(cpu, cpu->current);
    }
}


/*---------------------------------------------------------------------------
 *  Function: evt_fork_process
 *  Description: Event called when a new process is created. This would happen
 *  when the parent "calls" fork() in it's current cpu burst . 
 *  
 *  Param cpu: CPU in which the event happened
 *  Param priv: New process being created
 *  
 *  Yields events:
 *      - evt_reschedule (direct call)
 *--------------------------------------------------------------------------*/
void evt_fork_process(struct processor *cpu, void * priv) {
    struct process *p = (struct process *)priv;
    if (p->status != UNITIALIZED) {
        fprintf(stderr, "Forking process %d which is not UNITIALIZED\n", p->pid);
        exit(1);
    }
    
    fprintf(stderr, "[CPU %2d FORK at %10lld] %d -> %d\n",
            cpu->cpu_id,
            sim_current_time(),
            p->parent_pid,
            p->pid);

    // If not forking INIT and we either don't have a current or the current
    // is not the parent, it means that we did something wrong
    if (( p->pid != 0) && (!cpu->current || cpu->current->pid != p->parent_pid))
    {
        fprintf(stderr, "\t[FORK] Spurious fork! Current %d is not the parent of %d, %d is\n", 
                cpu->current? cpu->current->pid: -1,
                p->pid,
                p->parent_pid);
        exit(1);
    }

    p->status = READY;
    p->last_cpu = cpu->cpu_id;
    p->last_ready = sim_current_time();
    p->stats.sigma_waiting = 0;
    p->stats.migrations = 0;
    cpu->scheduler->enqueue(cpu->scheduler->priv, p);
    need_reschedule(cpu);
}



/*---------------------------------------------------------------------------
 *  Function: evt_block_process
 *  Description: Event called when a process blocks
 *  Param cpu: CPU in which the event happened
 *  Param priv: Process which will block
 *
 *  Yields events:
 *      - evt_wakeup_process -- After the current I/O burst is completed
 *      - evt_reschedule -- (called directly) to select a new process to run
 *--------------------------------------------------------------------------*/
void evt_block_process(struct processor * cpu, void * priv) {
    struct process * p = cpu->current;

    if (cpu->current != priv) {
        fprintf(stderr, "Ackward blocking process which is not current\n");
    }

    fprintf(stderr, "[CPU %2d BLOCK at %10lld] %d for %d\n",
            cpu->cpu_id,
            sim_current_time(),
            p->pid,
            p->io_bursts[p->cur_io_burst]);

    p->status = BLOCKED;

    // Block the current process
    cpu->scheduler->block(cpu->scheduler->priv, p);

    // Add an event to handle the WAKEUP after the current I/O burst
    struct event * evt = sim_new_event();
    if (!evt) {
        fprintf(stderr, "ERROR allocating new event: ENOMEM\n");
        exit(1);
    }

    evt->ts = sim_current_time() + p->io_bursts[p->cur_io_burst];
    evt->handle = evt_wakeup_process;
    evt->priv = p;
    evt->destroy = NULL;

    sim_add_event(&cpu->event_list, evt);

    // Selects a new one to run now
    need_reschedule(cpu);
}


/*---------------------------------------------------------------------------
 *  Function evt_wakeup_process
 *  Description: Event called when the I/O burst of a process is finished.
 *  Here we update the cur_io_burst and cur_cpu_burst of the process for a new
 *  epoch and zero any flag that must be reset for each epoch
 *  
 *  Param cpu: CPU in which the event happened
 *  Param priv: The process begin awaken
 *  
 *  Yields events:
 *       - evt_reschedule (called directly)
 *--------------------------------------------------------------------------*/
void evt_wakeup_process(struct processor *cpu, void *priv) {
    struct process * p = (struct process *)priv;

    fprintf(stderr, "[CPU %2d WAKEUP at %10lld] %d is READY\n",
            cpu->cpu_id,
            sim_current_time(),
            p->pid);

    // Updates it's current bursts
    p->cur_io_burst++;
    p->cur_cpu_burst++;
    p->children_created = 0;
    p->status = READY;
    p->last_ready = sim_current_time();

    // Tell the scheduler that it's awake
    cpu->scheduler->wake_up(cpu->scheduler->priv, p);
    need_reschedule(cpu);
}


/*---------------------------------------------------------------------------
 *  Function: evt_exit_processes
 *  Description: Event called when a process "calls" exit() (i.e. it's next
 *  cpu-burst has lenght -1
 *  
 *  Yields: 
 *      - evt_reschedule (called directly)
 *--------------------------------------------------------------------------*/
void evt_exit_processes(struct processor *cpu, void * priv) {
    struct process *p = (struct process *)priv;

    if (p->status == FINISHED) {
        fprintf(stderr, "ERROR Spurious exit for process %d\n", p->pid);
        exit(1);
    }

    if (p != cpu->current) {
        fprintf(stderr, "ERROR!: Current (%d) is not the one being finished (%d)\n",
                cpu->current ? cpu->current->pid : -1,
                p->pid);
        exit(1);
    }

    fprintf(stderr, "[CPU %2d EXIT at %10lld] %d is FINISHED\n",
            cpu->cpu_id,
            sim_current_time(),
            p->pid);

    p->status = FINISHED;

    cpu->scheduler->dequeue(cpu->scheduler->priv, p);
    need_reschedule(cpu);
}


/*---------------------------------------------------------------------------
 *  Function: fork_children
 *  Description: Creates FORK events for any children created in the current
 *  timeslice of the process p
 *  
 *  Yields events:
 *      - evt_fork_process : As many events as children created in the current
 *      timeslice
 *--------------------------------------------------------------------------*/
void fork_children(struct computer * c, struct processor * cpu, struct process * p) {
    int i;
    struct process * procs;
    int nprocs;

    if (!p)
        return;


    if (cpu != &(c->processors[c->cur_cpu])) {
        fprintf(stderr, "OOOOPS Forking children on wrong cpu despite event !?\n");
        exit(1);
    }

    procs = c->processes;
    nprocs = c->n_processes;

    for (i = 0; i < nprocs; i++) {
        if (procs[i].status == UNITIALIZED &&            // unforked process
            procs[i].parent_pid == p->pid &&            // created by p
            procs[i].created_on_parent_burst == p->cur_cpu_burst && // in this cpu burst
            procs[i].after < p->sigma_timeslice + p->timeslice) // before the end of the current timeslice
            // This is a pending child created during this timeslice
        {
            int when;
            struct process * child = &(procs[i]);

#ifdef SIMSCHED_DEBUG
            fprintf(stderr, "Creating children %d cpu burst %d after %d\n", 
                    child->pid,
                    child->created_on_parent_burst,
                    child->after);
            fprintf(stderr, "parend has sigma_timeslice of %lld, timeslice %lld\n", p->sigma_timeslice, p->timeslice);
#endif

            event_t * fork_evt = sim_new_event();
            if (!fork_evt) {
                fprintf(stderr, "ERROR allocating new event: ENOMEM\n");
                exit(1);
            }

            // When: Normalize after to the beginning ot the burst by removing
            // the sigma_timeslice
            when = sim_current_time() + child->after - p->sigma_timeslice;

            fork_evt->ts = when;
            fork_evt->handle = evt_fork_process;
            fork_evt->priv = child;
            fork_evt->destroy = NULL;

            if (!sim_add_event_no_dup(&cpu->event_list, fork_evt))
                fprintf(stderr, "\tCreating child %d at %lld\n", 
                        child->pid,
                        fork_evt->ts);
            
        }
    }
}


/*---------------------------------------------------------------------------
 *  Function: cancel_fork_children
 *  Description: Cancels the forks of any children by the process p
 *--------------------------------------------------------------------------*/
void cancel_fork_children(struct computer * c, struct processor * cpu, struct process * p) {
    int i;
    struct process * procs;
    int nprocs;

    if (!p)
        return;

    if (cpu != &(c->processors[c->cur_cpu])) {
        fprintf(stderr, "ERROR: Forking children on wrong cpu despite event !?\n");
        exit(1);
    }

    procs = c->processes;
    nprocs = c->n_processes;

    for (i = 0; i < nprocs; i++) {
        if (procs[i].status == UNITIALIZED &&            // unforked process
            procs[i].parent_pid == p->pid &&            // created by p
            procs[i].created_on_parent_burst == p->cur_cpu_burst) // in this cpu burst
        {
            // This is a child of p, created somewhere in the future but still
            // in the current cpu burst
            struct process * child = &(procs[i]);
            event_t *fork_evt = find_pending_event(c,   -1, 
                                                        evt_fork_process, 
                                                        child,
                                                        FIND_PENDING_BY_HANDLE|
                                                        FIND_PENDING_BY_PRIV);

            // If we had already scheduled the fork, cancel it
            // When evt_schedule happen, it will reschedule accordingly
            if (fork_evt)
                sim_cancel_event(fork_evt);

            fprintf(stderr, "\tCancelled fork of pid %d as %d lost the CPU\n", 
                    child->pid,
                    p->pid);
        }
    }
}

/*---------------------------------------------------------------------------
 *  Function: add_cpu_bust_end_evt
 *  Description: Adds an event at the end of the current timeslice -- either
 *  BLOCK, RESCHEDULE or EXIT
 *  
 *  Yields events:
 *      - evt_block: if process p blocks before it's given timeslice
 *      - evt_exit: if process p exits before it's given timeslice
 *      - evt_reschedule: if process p uses all it's timeslice before blocking
 *      or exiting
 *--------------------------------------------------------------------------*/
void add_cpu_bust_end_evt(struct processor *cpu, struct process *p) {
    sim_time_t ts_preempt,
               ts_block;
    if (!p) 
        return;

    fprintf(stderr, "\tAdding new events for process %d\n", p->pid);
    // Now schedule the end of the current CPU burst
    // It's a BLOCK or an EXIT after the current CPU burst
    
    ts_preempt = sim_current_time() + p->timeslice;
    ts_block = sim_current_time() + p->cpu_bursts[p->cur_cpu_burst] - p->sigma_timeslice;
   
    event_t * evt = sim_new_event();
    if (!evt) {
        fprintf(stderr, "ERROR allocating new event: ENOMEM\n");
        exit(1);
    }

    //if ((p->sigma_timeslice + p->timeslice ) >= p->cpu_bursts[p->cur_cpu_burst])
    if (ts_block <= ts_preempt)
    {
        // BLOCK before end of timeslice
        // Will execute until the end of the burst or the end of the process
        evt->ts = ts_block;
        if (p->io_bursts[p->cur_io_burst] == -1) {
            // last cpu burst, we'll schedule an EXIT
            evt->handle = evt_exit_processes;
        } else {
            // we have more i/o bursts, block
            evt->handle = evt_block_process;
        }
    } else {
        // Schedule a reschedule -- all the timeslices runned plus the current
        // won't finish the current cpu burst
        evt->handle = evt_reschedule;
        evt->ts = ts_preempt;
    }

    evt->priv = p;
    evt->destroy = NULL;

    if (!sim_add_event_no_dup(&cpu->event_list, evt)) {
        fprintf(stderr, "\tProcess %d will %s at %lld\n", 
                p->pid,
                (evt->handle == evt_block_process)? "BLOCK" : 
                (evt->handle == evt_exit_processes)? "EXIT" :
                                                "RESCHEDULE",
                evt->ts);
    } else {
        fprintf(stderr, "\tAvoiding dup event\n");
    }
}

void need_reschedule(struct processor *cpu) {
    event_t *resched = sim_new_event();

    if (!resched) {
        fprintf(stderr, "ERROR: NO memory for need resched\n");
        exit(1);
    }

    resched->ts = sim_current_time();
    resched->handle = evt_reschedule;
    resched->priv = NULL;
    resched->destroy = NULL;

    sim_add_event_no_dup(&cpu->event_list, resched);
}


/*---------------------------------------------------------------------------
 *  Function: name_common_evt
 *  Description: Returns a string with the name of the function pointed by f
 *  Used in functions that print events
 *--------------------------------------------------------------------------*/
const char * const name_common_evt(void * f) {
    if (f == evt_reschedule)
        return "evt_reschedule";
    else if (f == evt_fork_process)
        return "evt_fork_process";
    else if (f == evt_block_process)
        return "evt_block_process";
    else if (f == evt_exit_processes)
        return "evt_exit_processes";
    else if (f == evt_wakeup_process)
        return "evt_wakeup_process";

    return NULL;
}
