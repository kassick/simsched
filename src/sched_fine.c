/*
 * ===========================================================================
 *
 *       Filename:  sched_fine.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  16-05-2015 17:42:30
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <limits.h>
#include "list.h"
#include "sim.h"
#include "sim_cpu_engine.h"
#include "sched_fine.h"
#include "sim_common_events.h"



/*---------------------------------------------------------------------------
 *  First in, Never Out scheduller
 *  Once a process enters the CPU, it only leaves when it has finished
 *--------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
 *  struct fine_priv
 *  Description: This struct (pointed by scheduler->priv) holds the list of
 *  processes, shared by all CPUs
 *--------------------------------------------------------------------------*/
struct fine_priv {
    struct list_head process_list;
    struct computer * computer;
};


/*---------------------------------------------------------------------------
 *  struct fine_cpu_priv
 *  Description: Each CPU holds an instance of this struct, used to control
 *  the tick timing
 *--------------------------------------------------------------------------*/

struct fine_cpu_priv {
    struct process * current;
    sim_time_t last_tick;
};

/*---------------------------------------------------------------------------
 *  Function: fine_cpu_priv
 *  Description: Returns the fine_cpu_priv struct for the CPU
 *  Param cpu: The CPU whose fine_cpu_priv we need
 *  Return: The fine_cpu_priv struct for the CPU
 *--------------------------------------------------------------------------*/
static struct fine_cpu_priv * get_cpu_priv(struct processor * cpu) {
    return (struct fine_cpu_priv*)cpu->sched_info;
}


/*---------------------------------------------------------------------------
 *  struct fine_proc_holder
 *  Description: This is a struct associated with every process which is still
 *  alive (READY, RUNNING or BLOCKED). It allows us to have the process in a
 *  list within the fine_priv struct.
 *--------------------------------------------------------------------------*/
struct fine_proc_holder {
    struct list_head to_list;
    struct process * proc;
};


// fine tick function header, used by enqueue, wakeup
void fine_tick(void * priv, struct processor * cpu);

void fine_dispatch(void * priv, struct processor * cpu, struct process * p);


/*---------------------------------------------------------------------------
 *  Function: fine_wakeup_processor
 *  Description: Wakes up a processor that was idle
 *
 *  This function adds a reschedule event to the processor event queue. It
 *  assumes the processor was idle -- i.e. there was nothing scheduled for it
 *  at the current time
 *
 *  Param fp: fine priv struct
 *  Param cpu: CPU to wake up
 *
 *  Yields events:
 *      - evt_reschedule at cpu
 *--------------------------------------------------------------------------*/
void fine_wakeup_processor(struct processor *cpu) {
    event_t *evt = sim_new_event();
    if (!evt) {
        fprintf(stderr, "rr Error -- could not allocate new event for reschedule\n");
        exit(1);
    }

    fprintf(stderr, "\t[FINE Wake Up Processor %d]\n", cpu->cpu_id);

    evt->ts = sim_current_time();
    evt->handle = evt_reschedule;
    evt->priv = NULL;
    evt->destroy = NULL;

    if (!list_empty(&cpu->event_list) &&
        list_first_entry(&cpu->event_list, event_t, to_list)->ts <= sim_current_time())
    {
        fprintf(stderr, "rr WARNING: "
                        "Adding a reschedule event for NOW but there is "
                        "something pending there\n");
    }

    list_add(&evt->to_list, &cpu->event_list);
}



/*---------------------------------------------------------------------------
 *  Function: fine_enqueue
 *  Description: Adds a newly created process to the scheduler. THis function
 *  creates a new fine_proc_holder struct and stores it in the process
 *  sched_priv pointer. It also forces a tick in any idle CPU
 *--------------------------------------------------------------------------*/
void fine_enqueue(void * priv, struct process *p)
{
    int i;
    struct fine_priv *fpriv = (struct fine_priv*)priv;
    struct fine_proc_holder *fh = malloc(sizeof(struct fine_proc_holder));
    if (!fh) {
        fprintf(stderr, "ERROR: Could not allocate rr proc holder: ENOMEM\n");
        exit(1);
    }
    
    fprintf(stderr, "\t[FINE ENQUEUE] at %lld pid %d\n", sim_current_time(), p->pid);

    fh->proc = p;
    p->sched_priv_info = fh;

    list_add_tail(&fh->to_list, &fpriv->process_list);

    // Multi-CPU rr: Everytime a new process is enqueued, it ticks the first
    // available CPU
    for (i = 0; i < fpriv->computer->n_processors; i++) {
        if (!fpriv->computer->processors[i].current) {
            fine_wakeup_processor(&fpriv->computer->processors[i]);
            break;
        }
    }
}


/*---------------------------------------------------------------------------
 *  Function: fine_dequeue
 *  Description: Removes a process from the scheduler, destroy it's
 *  fine_proc_holder struct
 *--------------------------------------------------------------------------*/
// Removes a process from the queue and releases all it's related structs
void fine_dequeue(void * priv, struct process * p) {
    struct fine_proc_holder * p_fph = NULL;
    struct fine_priv *fp = (struct fine_priv*)priv;

    if (!p->sched_priv_info) {
        fprintf(stderr, "rr IMPL ERROR: Process not found in queue\n");
        exit(1);
    }

    fprintf(stderr, "\t[FINE dequeue] at %lld pid %d\n", sim_current_time(), p->pid);

    p_fph = (struct fine_proc_holder*)p->sched_priv_info;

    free(p_fph);

    p->sched_priv_info = NULL;
}



/*---------------------------------------------------------------------------
 *  Function: fine_wakeup_process
 *  Description: Wakes up a process, adds it to the end of the list. Also
 *  forces a tick in any idle processor
 *--------------------------------------------------------------------------*/
void fine_wakeup_process(void * priv, struct process *p)
{
    struct fine_priv * fp = priv;
    struct fine_proc_holder *fph;
    int i;

    if (!p->sched_priv_info) {
        fprintf(stderr, "ERROR: Process %d was woke up with no sched priv info\n", p->pid);
        exit(1);
    }

    p->sigma_timeslice = 0;

    fprintf(stderr, "\t[FINE wakeup] at %lld pid %d\n", sim_current_time(), p->pid);

    if (!fp->computer->processors[fp->computer->cur_cpu].current)
    {
        // this processor is idle
        // wake it up and avoid migrations
        fine_wakeup_processor(&fp->computer->processors[fp->computer->cur_cpu]);
    } else {
        // Multi-CPU FINE: Everytime a new process is enqueued, it ticks the first
        // available CPU
        for (i = 0; i < fp->computer->n_processors; i++) {
            if (!fp->computer->processors[i].current) {
                fine_wakeup_processor(&fp->computer->processors[i]);
                break;
            }
        }
    }
}


/*---------------------------------------------------------------------------
 *  Function: fine_block_process
 *  Description: Blocks a process -- i.e. removes it from the list
 *--------------------------------------------------------------------------*/
void fine_block_process(void * priv, struct process * p)
{
    struct fine_proc_holder *fph;
    
    if (!p->sched_priv_info) {
        fprintf(stderr, "ERROR: Process %d was blocked with no sched priv info\n", p->pid);
        exit(1);
    }

    fprintf(stderr, "\t[FINE block] at %lld pid %d\n", sim_current_time(), p->pid);
}

/*---------------------------------------------------------------------------
 *  Function: fine_tick
 *  Description: tick function for the rr scheduler. Updates the timeslice
 *  of the running process. If the timeslice is 0, selects a new process and
 *  dispatches it to the CPU
 *  May select a NULL process (in this case, the CPU goes idle)
 *--------------------------------------------------------------------------*/
void fine_tick(void * priv, struct processor * cpu) {
    struct fine_priv * fpriv = (struct fine_priv*)priv;

    fprintf(stderr, "\t[FINE TICK] at %lld\n", sim_current_time());
    if (!cpu->scheduler) {
        fprintf(stderr, "ERROR: Scheduler for CPU %d not set\n", cpu->cpu_id);
        exit(0);
    }

    get_cpu_priv(cpu)->last_tick = sim_current_time();

    // Once in, never out
    if (get_cpu_priv(cpu)->current && get_cpu_priv(cpu)->current->status != FINISHED) {
        // Do not dispatch blocked processes
        if (get_cpu_priv(cpu)->current->status == BLOCKED)
            cpu->scheduler->dispatch(cpu->scheduler->priv, cpu, NULL);
        else
            cpu->scheduler->dispatch(cpu->scheduler->priv, cpu, get_cpu_priv(cpu)->current);
    } else {
        struct process * p = NULL;
    
        p = cpu->scheduler->sched(cpu->scheduler->priv, cpu);
        fine_dispatch(priv, cpu, p);
    }
}


/*---------------------------------------------------------------------------
 *  Function: fine_sched
 *  Description: rr scheduler: Always selects the first process in the
 *  list
 *--------------------------------------------------------------------------*/
struct process * fine_sched(void * priv, struct processor * cpu)
{
    struct fine_priv* fp = (struct fine_priv*)priv;
    struct process * p = NULL;
    struct fine_proc_holder *fph;
    
    fprintf(stderr, "\t[FINE sched] at %lld for cpu %d\n", sim_current_time(), cpu->cpu_id);

    // Escolha: o primeiro da fila
    if (!list_empty(&fp->process_list)) {
        fph = list_first_entry(&fp->process_list, struct fine_proc_holder, to_list);
        return fph->proc;
    }
    
    if (!p)
        fprintf(stderr, "\t[FINE sched] at %lld Nothing for CPU %d\n", sim_current_time(), cpu->cpu_id);
    return p;
}


/*---------------------------------------------------------------------------
 *  Function: fine_dispatch
 *  Description: Dispatches a process in a CPU
 *--------------------------------------------------------------------------*/

void fine_dispatch(void * priv, struct processor * cpu, struct process * p)
{
    struct fine_priv *fp = priv;
    struct fine_proc_holder * fph;

    fprintf(stderr, "\t[FINE dispatch] at %lld to process %d\n", 
            sim_current_time(), 
            p? p->pid : -1);

    if (p) {
        get_cpu_priv(cpu)->current = p;
        p->last_activated = sim_current_time();
        p->status = RUNNING;
        p->timeslice = INT_MAX;
        fph = (struct fine_proc_holder*)p->sched_priv_info;
        // get it out of the CPU just to be sure no other processor takes it
        // away
        list_del_init(&fph->to_list);
    } else
            fprintf(stderr, "\t[FINE dispatch] at %lld going idle on cpu %d\n", sim_current_time(), cpu->cpu_id);

    cpu->current = p;
}

/*---------------------------------------------------------------------------
 *  Function: fine_terminate
 *  Description: Called when the simulater finishes -- handy place to print
 *  stats
 *--------------------------------------------------------------------------*/
void fine_terminate(void * priv) {
    struct fine_priv * rp = (struct fine_priv*)priv;
    struct computer * c = sim_computer();
    int i;

    for ( i = 0; i < c->n_processors; i++) {
        printf("Processor %d: idle: %lld busy: %lld\n", i, 
                c->processors[i].idle_time,
                c->processors[i].ocupied_time);
    }
}

/*---------------------------------------------------------------------------
 *  Function: fine_create_scheduler
 *
 *  Param c: The computer
 *  Return: A newly created scheduler struct
 *--------------------------------------------------------------------------*/
struct scheduler * fine_create_scheduler(struct computer * c, int argc, char**argv)
{
    int i;
    struct scheduler * sched = malloc(sizeof(struct scheduler));
    if (!sched)
        return NULL;

    struct fine_priv *priv = malloc(sizeof(struct fine_priv));
    if (!priv) {
        free(sched);
        return NULL;
    }

    // For this simulated computer
    priv->computer = c;

    // initialize list as empty
    priv->process_list.next =
        priv->process_list.prev =
        &(priv->process_list);


    sched->priv = priv;
    sched->enqueue = fine_enqueue;
    sched->dequeue = fine_dequeue;
    sched->block = fine_block_process;
    sched->wake_up = fine_wakeup_process;
    sched->sched = fine_sched;
    sched->dispatch = fine_dispatch;
    sched->tick = fine_tick;
    sched->terminate = fine_terminate;

    for (i = 0; i < c->n_processors; i++) {
        c->processors[i].scheduler = sched;
        c->processors[i].sched_info = malloc(sizeof(struct fine_cpu_priv));
        get_cpu_priv(&c->processors[i])->last_tick = 0;
        get_cpu_priv(&c->processors[i])->current = NULL;
    }

    fprintf(stderr, "Created First-in Never-out scheduler\n");

    return sched;
}
