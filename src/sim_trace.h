/*
 * ===========================================================================
 *
 *       Filename:  sim_trace.h
 *
 *    Description:  Simulation trace parser
 *
 *        Version:  1.0
 *        Created:  27-05-2015 09:34:21
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIM_TRACE_H__
#define __SIM_TRACE_H__ 1

#include "sim_computer.h"
struct process * parse_trace(const char * fname, int * n_procs);
int parse_affinity(struct computer * c, int n_cpus, const char * const fname);

#endif
