/*
 * ===========================================================================
 *
 *       Filename:  sim_process.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  27-05-2015 09:33:02
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */

#ifndef __SIM_PROCESS_H__
#define __SIM_PROCESS_H__ 1

#include "sim_cpu_engine.h"

enum process_status {
    UNITIALIZED = 0,
    READY,
    RUNNING,
    BLOCKED,
    FINISHED
};

struct process_stats {

    // HOw many migrations from one processor to the other
    int migrations;

    // How much time spent waiting
    sim_time_t sigma_waiting;
};

struct process {
    enum process_status status;
    int pid;
    int parent_pid;
    int created_on_parent_burst;
    int after;
    int prio;
    int *cpu_bursts, n_cpu_bursts; // vetor de tempo de duração dos bursts CPU
    int *io_bursts, n_io_bursts; // vetor de tempo de duração dos bursts IO

    int cur_cpu_burst, cur_io_burst;
    int children_created;

    sim_time_t ts_next_event;

    int last_cpu;
    sim_time_t timeslice,
               sigma_timeslice;
    sim_time_t last_activated,
               last_ready;

    struct process_stats stats;

    // anything the scheduller wants for this process
    void * sched_priv_info;
};


#endif
