/*
 * ===========================================================================
 *
 *       Filename:  sim_logger.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  22-06-2015 09:03:20
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include "sim_logger.h"
#include "sim_cpu_engine.h"

FILE * sim_log_file = NULL;

FILE * sim_log_open(const char * const fname) {
    FILE * f = fopen(fname, "w+");
    if (!f) {
        fprintf(stderr, "ERROR: Could not open log file %s\n", fname);
        return NULL;
    }

    sim_log_file = f;

    return f;
}

void sim_log_resched(int cpu_id, int from_pid, int to_pid) {
    if (sim_log_file)
        fprintf(sim_log_file, "%d %lld %d %d\n",
                cpu_id,
                sim_current_time(),
                from_pid, to_pid);
}

void sim_log_close() {
    if (sim_log_file) {
        fclose(sim_log_file);
    }
}
