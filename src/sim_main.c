/*
 * ===========================================================================
 *
 *       Filename:  sim_sched.c
 *
 *    Description:  Main function
 *
 *        Version:  1.0
 *        Created:  16-05-2015 16:14:20
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Virote Kassick (), kassick@gmail.com
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>

#include "sim.h"
#include "sim_common_events.h"
#include "sim_trace.h"
#include "sim_logger.h"

#include "sched_fifo.h"
#include "sched_fine.h"
#include "sched_rr.h"
#include "sched_dummy.h"


/*---------------------------------------------------------------------------
 *  Option parsing
 *--------------------------------------------------------------------------*/
struct sched_opt_entry {
    char * name, *desc;
    create_scheduler_fun_t create_fun;
    print_help_fun_t help;
};

const struct sched_opt_entry available_schedulers[] = {
    {"fifo", "First-in First-out scheduller", fifo_create_scheduler, NULL},
    {"fine", "First-in Never-out scheduller", fine_create_scheduler, NULL},
    {"rr", "Round-Robin scheduller", rr_create_scheduler, rr_print_help},
    {"dummy", "DUMMY DOES NOTHING\n", dummy_create_scheduler, dummy_print_help},
    {NULL, NULL, NULL, NULL},
};


void print_help() {
    int i;
    printf( "Usage: sim_sched [-a <affinity_file>] -n <n_cpus> -s <sched> -t <input_trace>\n"
            "   -a <affinity_file>          : File describing CPU affinity\n"
            "   -n <n_cpus>                 : Number of CPUs\n"
            "   -s <sched>|--sched <sched>  : Scheduler to use\n"
            "   -t <input_trace>            : Input trace file\n"
            "   -o <log_file>               : Schedulling log file\n"
            "   --list-sched                : List available schedulers\n"
            "   -h|--help                   : Print this message\n"
          );

    for (i = 0; available_schedulers[i].name; i++) {
        if (available_schedulers[i].help) {
            printf("\nHelp for scheduler %s:\n", available_schedulers[i].name);
            available_schedulers[i].help();
        }
    }
}

create_scheduler_fun_t find_sched(const char * name) {
    int i;
    for ( i = 0; available_schedulers[i].name; i++)
        if (!strcmp(available_schedulers[i].name, name))
            return available_schedulers[i].create_fun;
    return NULL;
}

void list_sched() {
    int i;
    int maxlen = 0;

    printf("Available schedulers:\n");

    for (i = 0; available_schedulers[i].name; i++) {
        if (strlen(available_schedulers[i].name) > maxlen) {
            maxlen = strlen(available_schedulers[i].name);
        }
    }

    for (i = 0; available_schedulers[i].name; i++) {
        printf("%*s : %s\n",    maxlen, 
                                available_schedulers[i].name,
                                available_schedulers[i].desc);
    }
}

struct sim_cmdline_opts {
    int n_cpus;
    char * trace_fname;
    char * affinity_fname;
    char * log_output_file;
    create_scheduler_fun_t create_sched;
};


struct sim_cmdline_opts parse_options(int argc, char**argv) {
    static struct option long_options[] =
        { {"sched", required_argument,  NULL, 's'},
          {"help",  no_argument,        NULL, 'h'},
          {"list-sched", no_argument, NULL, 0},
          {NULL, 0, 0, 0},
        };

    static const char *shortopts = "t:a:n:s:ho:";

    int opt, longindex;
    int cur_opt_i = 1;

    struct sim_cmdline_opts ret;
    ret.create_sched = NULL;
    ret.n_cpus = 0;
    ret.trace_fname = ret.affinity_fname = ret.log_output_file = NULL;

    while(1) {
        opt = getopt_long(argc, argv, shortopts, long_options , &longindex);
        if (opt == -1)
            break;

        if (opt && opt != '?')
            argv[cur_opt_i++][0] = 0;

        switch(opt) {
            case 'h':
                print_help();
                exit(0);
                break;
            case 's':
                ret.create_sched = find_sched(optarg);
                argv[cur_opt_i++][0] = 0;
                if (!ret.create_sched) {
                    fprintf(stderr, "Could not find specified sched. "
                                    "Available schedulers are:\n");
                    list_sched();
                    exit(1);
                }
                break;
            case 'n':
                ret.n_cpus = atoi(optarg);
                argv[cur_opt_i++][0] = 0;
                break;
            case 't':
                ret.trace_fname = strdup(optarg);
                argv[cur_opt_i++][0] = 0;
                break;
            case 'a':
                ret.affinity_fname = strdup(optarg);
                argv[cur_opt_i++][0] = 0;
                break;
            case 'o':
                ret.log_output_file = strdup(optarg);
                argv[cur_opt_i++][0] = 0;
                break;
            case 0:
                argv[cur_opt_i++][0] = 0;
                if (!strcmp(long_options[longindex].name, "help")) {
                   print_help();
                   exit(0);
                } else if (!strcmp(long_options[longindex].name, "sched")) {
                    ret.create_sched = find_sched(optarg);
                    argv[cur_opt_i++][0] = 0;
                    if (!ret.create_sched) {
                        fprintf(stderr, "Could not find specified sched. "
                                        "Available schedulers are:\n");
                        list_sched();
                        exit(1);
                    }
                } else if (!strcmp(long_options[longindex].name, "list-sched")) {
                    list_sched();
                    exit(0);
                } /*else {
                    print_help();
                    exit(1);
                }*/
                break;
                /*
            default:
                print_help();
                exit(1);*/
        }
    }

    optind = 1;
    return ret;
}



/*---------------------------------------------------------------------------
 *  Function: sanitize_argv
 *  Description: Creates a new argv only with non-empty entries
 *--------------------------------------------------------------------------*/
void sanitize_argv(int * argc, char***argv) {
    int i,
        new_argc = 0;
    char** new_argv = malloc((*argc) * sizeof(char*));

    for (i = 0; i < (*argc); i++) {
        if ( (*argv)[i][0] ) {
            new_argv[new_argc++] = (*argv)[i];
        }
    }

    *argc = new_argc;
    *argv = new_argv;
}



/*---------------------------------------------------------------------------
 *  Function: main
 *  Description: Parses options, reads the input files, creates the scheduler
 *  and "forks" INIT -- the process with pid 0
 *--------------------------------------------------------------------------*/


int main(int argc, char *argv[])
{
    struct sim_cmdline_opts opts = parse_options(argc, argv);
    struct computer c;
    int i;

    if (!opts.trace_fname) {
#ifdef UNIX
        opts.trace_fname = "/dev/stdin";
#else
        fprintf(stderr, "Error: Can not do anything without a trace file\n");
        exit(1);
#endif
    }

    if ((opts.n_cpus < 1) && (!opts.affinity_fname)) {
        fprintf(stderr, "Can not work on 0 CPUs\n");
        exit(1);
    }

    printf("PArsing affinity\n");
    if (parse_affinity(&c, opts.n_cpus, opts.affinity_fname)) {
        fprintf(stderr, "Invaild CPU configuration\n");
        exit(1);
    }

    printf("Parsing trace\n");
    c.processes = parse_trace(opts.trace_fname, &c.n_processes);
    if (!c.processes) {
        fprintf(stderr, "Could not parse input trace\n");
        exit(1);
    }

    printf("Files parsed\n");

    if (c.processes[0].pid != 0) {
        fprintf(stderr, "Trace must begin with process 0 , or init\n");
        exit(1);
    }

#ifdef SIMSCHED_DEBUG
    dump_trace(c.processes, c.n_processes);
#endif

    if (opts.log_output_file && !sim_log_open(opts.log_output_file)) {
        fprintf(stderr, "Could not open log file, abordint\n");
        exit(1);
    }


    sanitize_argv(&argc, &argv);
#ifdef SIMSCHED_DEBUG
    fprintf(stderr, "Has %d arguments in sanitizes:\n", argc);
    for (i = 0; i < argc; i++) {
        fprintf(stderr, "Sanitized argv[%d] = %s\n", i, argv[i]);
    }
#endif

    // Creates the scheduler
    //
   
    if (!opts.create_sched) {
        fprintf(stderr, "Error! No valid scheduler selected\n");
        exit(1);
    }

    c.scheduler = opts.create_sched(&c, argc, argv);
    if (!c.scheduler) {
        fprintf(stderr, "ERROR creating scheduler\n");
        exit(1);
    }


    sim_init(&c);

    // This gives the CPU for the INIT processes
    // A kernel would do this somehow
   
    // "Fork" init process
    evt_fork_process(&c.processors[0], &c.processes[0]);

    fprintf(stderr, "\n"
                    "-------------------------------------------------------------------------------\n"
                    "\n"
                    "                               Simulation starts NOW\n"
                    "\n"
                    "-------------------------------------------------------------------------------\n"
                    "\n");

    sim_main_loop(&c, name_common_evt);

    fprintf(stderr, "\n"
                    "-------------------------------------------------------------------------------\n"
                    "\n"
                    "\tSimulation Finished\n"
                    "\tCurrent time: %lld\n"
                    "\n"
                    "-------------------------------------------------------------------------------\n"
                    "\n", sim_current_time());

    for (i = 0; i < c.n_processes; i++) {
        struct process * p = &(c.processes[i]);
        printf("Process %d: %d migrations\t%lld waiting\n",
                p->pid,
                p->stats.migrations,
                p->stats.sigma_waiting);
    }

    if (c.scheduler->terminate)
        c.scheduler->terminate(c.scheduler->priv);

    sim_log_close();

    return 0;
}
